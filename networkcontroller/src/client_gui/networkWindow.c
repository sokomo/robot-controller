/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>
#include <client_network.h>
#include <common_define.h>

extern GtkWidget *gNetworkControlWindow;
extern GtkBuilder *gMainWindowBuilder;

struct entry_data
{
  GtkWidget *entryIpAddr;
  GtkWidget *entryPort;
};

struct entry_data entrydt;
extern int socketfd;


int
readIPFromFile(char *ipAddr, unsigned int *portNum)
{
  int filefd = -1;
  unsigned int limitData = 100;
  char fileName[] = "ipconnection.conf";
  char strData[limitData];

  if ((filefd = open(fileName, O_RDONLY)) < 0)
  {
    print_errno_message();
    return -1;
  }

  if (read(filefd, strData, limitData) < 0)
  {
    print_errno_message();
    close(filefd);
    return -1;
  }

  sscanf(strData, "%s %d", ipAddr, portNum);
  close(filefd);

  return 1;
}

int
writeIPToFile(const char *ipAddr, unsigned int *portNum)
{
  int filefd = -1;
  unsigned int limitData = 100;
  char fileName[] = "ipconnection.conf";
  char strData[limitData];

  if ((filefd = open(fileName, O_WRONLY)) < 0)
  {
    print_errno_message();
    return -1;
  }

  sprintf(strData, "%s %d", ipAddr, *portNum);
  
  if (write(filefd, strData, limitData) < 0)
  {
    print_errno_message();
    close(filefd);
    return -1;
  }
  
  close(filefd);

  return 1;
}


void
load_data_ip()
{
  char ipAddrStr[30];
  char portStr[6];
  unsigned int portNum = 0;

  if (readIPFromFile(ipAddrStr, &portNum) > 0)
  {
    sprintf(portStr, "%d", portNum);
    gtk_entry_set_text((GtkEntry *) entrydt.entryIpAddr, ipAddrStr);
    gtk_entry_set_text((GtkEntry *) entrydt.entryPort, portStr);
  }

}


void
set_image_simple(GtkImage* image, char* pathName)
{
  GdkPixbuf *imgPixbuf;
  //GError gerror[10];

  imgPixbuf = gdk_pixbuf_new_from_file_at_scale(pathName,
                                                141, 141,
                                                FALSE, NULL);

  gtk_image_set_from_pixbuf(image, imgPixbuf);
}


/* ******************************************************************* */
void
cancel_connection(GtkWidget *widget, gpointer data)
{
  
  gtk_widget_destroy(gNetworkControlWindow);
  gNetworkControlWindow = NULL;
}

void
begin_connection(GtkWidget *widget, gpointer data)
{
  GtkWidget *widgetMenuItem;
  const char* ipAddrStr =  gtk_entry_get_text((GtkEntry *) entrydt.entryIpAddr);
  unsigned int portNum = 0;
  portNum = atoi(gtk_entry_get_text((GtkEntry *) entrydt.entryPort));
  create_network_socket(&socketfd, ipAddrStr, portNum);

  if (socketfd > 0)
  {
    widgetMenuItem = GTK_WIDGET(gtk_builder_get_object(gMainWindowBuilder, "connectImagemenuitem"));
    gtk_widget_set_sensitive(widgetMenuItem, 0);

    widgetMenuItem = GTK_WIDGET(gtk_builder_get_object(gMainWindowBuilder, "disconnectImagemenuitem"));
    gtk_widget_set_sensitive(widgetMenuItem, 1);

    widgetMenuItem = GTK_WIDGET(gtk_builder_get_object(gMainWindowBuilder, "deviceconnectImagemenuitem"));
    gtk_widget_set_sensitive(widgetMenuItem, 1);

    writeIPToFile(ipAddrStr, &portNum);

    gtk_widget_destroy(gNetworkControlWindow);
    gNetworkControlWindow = NULL;
  }
}

void
set_signal_button_connection(GtkBuilder *builder)
{
  GtkWidget *widget;
  
  widget = GTK_WIDGET(gtk_builder_get_object(builder, "entryIpAddr"));
  entrydt.entryIpAddr = widget;

  widget = GTK_WIDGET(gtk_builder_get_object(builder, "entryPort"));
  entrydt.entryPort = widget;


  widget = GTK_WIDGET(gtk_builder_get_object(builder, "buttonConnect"));
  g_signal_connect(widget, "clicked", G_CALLBACK(begin_connection), NULL);


  widget = GTK_WIDGET(gtk_builder_get_object(builder, "buttonCancel"));
  g_signal_connect(widget, "clicked", G_CALLBACK(cancel_connection), NULL);

}


void
initial_connect_signal(GtkBuilder *builder)
{
  if (builder == NULL)
  {
    print_error_normal_message("No builder resource.\n");
    return;
  }
 
  set_signal_button_connection(builder);
  load_data_ip();
}


void
gtk_network_window_initial()
{
  GtkBuilder *builder;

  if (gNetworkControlWindow != NULL)
    return;


  /* Construct a GtkBuilder instance and load our UI description */
  builder = gtk_builder_new ();
  gtk_builder_add_from_file (builder, "network_connect.ui", NULL);

  /* Connect signal handlers to the constructed widgets. */
  gNetworkControlWindow = GTK_WIDGET( gtk_builder_get_object (builder, "connectWindow"));
  g_signal_connect (gNetworkControlWindow, "destroy", G_CALLBACK (cancel_connection), NULL);

  initial_connect_signal(builder);

  g_object_unref(G_OBJECT(builder));
  gtk_widget_show_all(gNetworkControlWindow);

}
