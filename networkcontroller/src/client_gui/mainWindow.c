/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <common_define.h>
#include <client_network.h>
#include <client_rb_backend.h>
#include "networkWindow.h"


int motorlist[5] = { 1, 2, 3, 4, 5};

GtkWidget *gNetworkControlWindow;
GtkBuilder *gMainWindowBuilder;
extern int socketfd;


static void
set_image_simple(GtkImage* image, char* pathName)
{
  GdkPixbuf *imgPixbuf;
  //GError gerror[10];

  imgPixbuf = gdk_pixbuf_new_from_file_at_scale(pathName,
                                                141, 141,
                                                FALSE, NULL);

  gtk_image_set_from_pixbuf(image, imgPixbuf);
}


static void
initial_load_image(GtkBuilder* builder)
{
  GtkImage *image;
  if (builder == NULL)
  {
    print_error_normal_message("No builder resource.\n");
    return;
  }

  image = (GtkImage*) GTK_WIDGET(gtk_builder_get_object(builder,
                                                        "imagePlinth"));
  set_image_simple(image, "image/RobotArm1.png");

  image = (GtkImage*) GTK_WIDGET(gtk_builder_get_object(builder,
                                                        "imageJoint1"));
  set_image_simple(image, "image/RobotArm2.png");

  image = (GtkImage*) GTK_WIDGET(gtk_builder_get_object(builder,
                                                        "imageJoint2"));
  set_image_simple(image, "image/RobotArm3.png");

  image = (GtkImage*) GTK_WIDGET(gtk_builder_get_object(builder,
                                                        "imageNeck"));
  set_image_simple(image, "image/RobotArm4.png");

  image = (GtkImage*) GTK_WIDGET(gtk_builder_get_object(builder,
                                                        "imageTongs"));
  set_image_simple(image, "image/RobotArm5.png");

}

/* Adjment value bar */

static void
adj_value_change(GObject *widget, gpointer data)
{
  //struct datamotor *motorInfo = (struct datamotor*) data;
  int motorid = *(int*) data;
  GtkAdjustment *adjPos = (GtkAdjustment*) widget;
  int curPos = gtk_adjustment_get_value(adjPos);
  choose_command_to_move(motorid, curPos);
}


static void
set_signal_adjment_value_changed(GtkBuilder *builder)
{
  GObject *widget;

  /* Adjustment widget */
  widget = gtk_builder_get_object(builder, "adjPosPlinth");
  g_signal_connect(widget, "value-changed", G_CALLBACK(adj_value_change), motorlist);

  widget = gtk_builder_get_object(builder, "adjPosJoint1");
  g_signal_connect(widget, "value-changed", G_CALLBACK(adj_value_change), motorlist + 1);

  widget = gtk_builder_get_object(builder, "adjPosJoint2");
  g_signal_connect(widget, "value-changed", G_CALLBACK(adj_value_change), motorlist + 2);

  widget = gtk_builder_get_object(builder, "adjPosNeck");
  g_signal_connect(widget, "value-changed", G_CALLBACK(adj_value_change), motorlist + 3);

  widget = gtk_builder_get_object(builder, "adjPosTongs");
  g_signal_connect(widget, "value-changed", G_CALLBACK(adj_value_change), motorlist + 4);
}

static void
set_default_adj_value(GObject *widget, gpointer data)
{
  int motorid = *(int*) data;
  GtkAdjustment *adjPos;

  switch (motorid)
  {
  case 1:
    adjPos = (GtkAdjustment *) gtk_builder_get_object(gMainWindowBuilder, "adjPosPlinth");
    gtk_adjustment_set_value(adjPos, 512);
    break;

  case 2:
    adjPos = (GtkAdjustment *) gtk_builder_get_object(gMainWindowBuilder, "adjPosJoint1");
    gtk_adjustment_set_value(adjPos, 512);
    break;

  case 3:
    adjPos = (GtkAdjustment *) gtk_builder_get_object(gMainWindowBuilder, "adjPosJoint2");
    gtk_adjustment_set_value(adjPos, 512);
    break;

  case 4:
    adjPos = (GtkAdjustment *) gtk_builder_get_object(gMainWindowBuilder, "adjPosNeck");
    gtk_adjustment_set_value(adjPos, 512);
    break;

  case 5:
    adjPos = (GtkAdjustment *) gtk_builder_get_object(gMainWindowBuilder, "adjPosTongs");
    gtk_adjustment_set_value(adjPos, 375);
    break;

  default:
    fprintf(stdout, "Currently no button available.\n");
  }

}

static void
set_signal_default_button(GtkBuilder *builder)
{
  GtkWidget *widget;

  /* Default button widget */
  widget = GTK_WIDGET(gtk_builder_get_object(builder, "buttonDefaultPlinth"));
  g_signal_connect(widget, "clicked", G_CALLBACK(set_default_adj_value), motorlist);

  widget = GTK_WIDGET(gtk_builder_get_object(builder, "buttonDefaultJoint1"));
  g_signal_connect(widget, "clicked", G_CALLBACK(set_default_adj_value), motorlist + 1);

  widget = GTK_WIDGET(gtk_builder_get_object(builder, "buttonDefaultJoint2"));
  g_signal_connect(widget, "clicked", G_CALLBACK(set_default_adj_value), motorlist + 2);

  widget = GTK_WIDGET(gtk_builder_get_object(builder, "buttonDefaultNeck"));
  g_signal_connect(widget, "clicked", G_CALLBACK(set_default_adj_value), motorlist + 3);

  widget = GTK_WIDGET(gtk_builder_get_object(builder, "buttonDefaultTongs"));
  g_signal_connect(widget, "clicked", G_CALLBACK(set_default_adj_value), motorlist + 4);
}


/* Network Menu */
static void
open_network_window(GtkMenuItem *menuItem, gpointer data)
{
  if (socketfd < 0)
  {
    gtk_network_window_initial();
  }
}

static void
close_network_connect(GtkMenuItem *menuItem, gpointer data)
{
  GtkWidget *widgetMenuItem;

  if (socketfd < 0)
  {
    print_error_normal_message("Not connecting to the server.\n");
    return;
  }
  close_network_socket(&socketfd);

  widgetMenuItem = GTK_WIDGET(gtk_builder_get_object(gMainWindowBuilder, "connectImagemenuitem"));
  gtk_widget_set_sensitive(widgetMenuItem, 1);

  widgetMenuItem = GTK_WIDGET(gtk_builder_get_object(gMainWindowBuilder, "disconnectImagemenuitem"));
  gtk_widget_set_sensitive(widgetMenuItem, 0);

  fprintf(stdout, "Close the connection.\n");
}


/* Device menu */
static void
close_device_connect(GtkMenuItem *menuItem, gpointer data)
{
  GtkWidget *widgetMenuItem;

  close_serial_device();

  widgetMenuItem = GTK_WIDGET(gtk_builder_get_object(gMainWindowBuilder, "deviceconnectImagemenuitem"));
  gtk_widget_set_sensitive(widgetMenuItem, 1);

  widgetMenuItem = GTK_WIDGET(gtk_builder_get_object(gMainWindowBuilder, "devicedisconnectImagemenuitem"));
  gtk_widget_set_sensitive(widgetMenuItem, 0);

}

static void
open_device_connect(GtkMenuItem *menuItem, gpointer data)
{
  GtkWidget *widgetMenuItem;

  open_serial_device();

  widgetMenuItem = GTK_WIDGET(gtk_builder_get_object(gMainWindowBuilder, "deviceconnectImagemenuitem"));
  gtk_widget_set_sensitive(widgetMenuItem, 0);

  widgetMenuItem = GTK_WIDGET(gtk_builder_get_object(gMainWindowBuilder, "devicedisconnectImagemenuitem"));
  gtk_widget_set_sensitive(widgetMenuItem, 1);

}

/* Detroy window and memory */
static void
destroy_full_window(GtkWidget *widget, gpointer data)
{
  close_serial_device();        /* Close device */
  if (socketfd > 0)
    close_network_socket(&socketfd); /* Close connect socket */

  gtk_main_quit();              /* Quit main window */
  g_object_unref(G_OBJECT(gMainWindowBuilder)); /* Remove reference builder object */
}


/* Set group menu action */
static void
set_menu_signal(GtkBuilder *builder)
{
  GtkWidget *widget;

  widget = GTK_WIDGET(gtk_builder_get_object(builder, "connectImagemenuitem"));
  g_signal_connect(widget, "activate", G_CALLBACK(open_network_window), NULL);

  widget = GTK_WIDGET(gtk_builder_get_object(builder, "disconnectImagemenuitem"));
  g_signal_connect(widget, "activate", G_CALLBACK(close_network_connect), NULL);

  widget = GTK_WIDGET(gtk_builder_get_object(builder, "deviceconnectImagemenuitem"));
  g_signal_connect(widget, "activate", G_CALLBACK(open_device_connect), NULL);

  widget = GTK_WIDGET(gtk_builder_get_object(builder, "devicedisconnectImagemenuitem"));
  g_signal_connect(widget, "activate", G_CALLBACK(close_device_connect), NULL);
}


static void
initial_connect_signal(GtkBuilder *builder)
{
  GtkWidget *widget;
  if (builder == NULL)
  {
    print_error_normal_message("No builder resource.\n");
    return;
  }
 
  set_signal_adjment_value_changed(builder);
  set_signal_default_button(builder);
  set_menu_signal(builder);

  widget = GTK_WIDGET(gtk_builder_get_object(builder, "quitImagemenuitem"));
  g_signal_connect(widget, "activate", G_CALLBACK(destroy_full_window), NULL);
  
}


inline static void
gtk_control_window_initial(int* argc, char*** argv)
{
  /* GtkBuilder *builder; */
  GtkWidget *window;
  gNetworkControlWindow = NULL;
  socketfd = -1;

  gtk_init (argc, argv);

  /* Construct a GtkBuilder instance and load our UI description */
  gMainWindowBuilder = gtk_builder_new ();
  gtk_builder_add_from_file (gMainWindowBuilder, "network_control.ui", NULL);

  /* Connect signal handlers to the constructed widgets. */
  window = GTK_WIDGET( gtk_builder_get_object (gMainWindowBuilder, "mainWindow"));
  g_signal_connect (window, "destroy", G_CALLBACK (destroy_full_window), NULL);

  initial_load_image(gMainWindowBuilder);
  initial_connect_signal(gMainWindowBuilder);

  gtk_widget_show_all(window);

  gtk_main ();
}


int
main (int   argc,
      char *argv[])
{
  
  gtk_control_window_initial(&argc, &argv);
  return 0;
}
