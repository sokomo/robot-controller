/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <termio.h>
#include <unistd.h>
#include <dynamixel_server.h>
#include <time.h>
#include <define_id_motors.h>
#include <common_define.h>


// Control table address
#define P_GOAL_POSITION_L        30
#define P_GOAL_POSITION_H       31

#define P_GOAL_SPEED_L		      32
#define P_GOAL_SPEED_H           33

#define P_TORQUE_L                   34
#define P_TORQUE_H                  35

#define P_PRESENT_POSITION_L	36
#define P_PRESENT_POSITION_H	37

#define P_MOVING		46

#define P_GOAL_LIMIT_L              0
#define P_GOAL_LIMIT_H              1023


int getDeviceInfoFile(char *deviceName, unsigned int *baudNum)
{
  int filefd = -1;
  unsigned int limitData = 100;
  char fileName[] = "deviceDXL.conf";
  char strData[limitData];

  if ((filefd = open(fileName, O_RDONLY)) < 0)
  {
    print_errno_message();
    return -1;
  }

  if (read(filefd, strData, limitData) < 0)
  {
    print_errno_message();
    close(filefd);
    return -1;
  }

  sscanf(strData, "%s %d", deviceName, baudNum);
  close(filefd);

  return 1;
}


void printCommStatus(int CommStatus)
{
    switch(CommStatus)
	{
	case COMM_TXFAIL:
		printf("COMM_TXFAIL: Failed transmit instruction packet!\n");
		break;

	case COMM_TXERROR:
		printf("COMM_TXERROR: Incorrect instruction packet!\n");
		break;

	case COMM_RXFAIL:
		printf("COMM_RXFAIL: Failed get status packet from device!\n");
		break;

	case COMM_RXWAITING:
		printf("COMM_RXWAITING: Now receiving status packet!\n");
		break;

	case COMM_RXTIMEOUT:
		printf("COMM_RXTIMEOUT: There is no status packet!\n");
		break;

	case COMM_RXCORRUPT:
		printf("COMM_RXCORRUPT: Incorrect status packet!\n");
		break;

	default:
		printf("This is unknown error code!\n");
		break;
	}
}

void printErrorCode()
{
    if(dxl_get_rxpacket_error(ERRBIT_VOLTAGE) == 1)
		printf("Input voltage error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_ANGLE) == 1)
		printf("Angle limit error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_OVERHEAT) == 1)
		printf("Overheat error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_RANGE) == 1)
		printf("Out of range error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_CHECKSUM) == 1)
		printf("Checksum error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_OVERLOAD) == 1)
		printf("Overload error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_INSTRUCTION) == 1)
		printf("Instruction code error!\n");
}

int test_Comm_Status()
{
    int CommStatus;
    CommStatus = dxl_get_result();
    if( CommStatus == COMM_RXSUCCESS )
    {
        printErrorCode();
        return 1;
    }
    else
    {
        printCommStatus(CommStatus);
        return 0;
    }
}


int open_serial_device(char* deviceName, int baudNum)
{
  unsigned int baudNumFile = 0;
  char deviceNameFile[30];
  int flagResult = 0;

  if (getDeviceInfoFile(deviceNameFile, &baudNumFile) > 0)
    flagResult = dxl_initialize(deviceNameFile, baudNumFile);

  if (flagResult == 0)
    flagResult = dxl_initialize(deviceName, baudNum);

  if ( flagResult == 0 )
  {
      printf( "Failed to open device %s\n", deviceName );
      return 0;
  }
  else
      printf( "Succeed to open USB2Dynamixel!\n" );
  return 1;
}

void close_serial_device()
{
    dxl_terminate();
}


void choose_packet_type(unsigned char *packetData)
{
  switch (packetData[0])
  {
  case COMMAND_DYNAMIXEL:
    if (packetData[1] == COMMAND_CONNECT)
      open_serial_device("/dev/ttyUSB0", 57600);
    if (packetData[1] == COMMAND_DISCONNECT)
      close_serial_device();
    break;
  case COMMAND_PACKAGE:
    dxl_txrx_packet(packetData);
    test_Comm_Status();
      break;
  default:
    fprintf(stdout, "Receive wrong packet data.\n");
    break;
  }
}
