/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <client_network.h>

/* Get socket address (IPv4 and IPv6) */
void *get_in_addr(struct sockaddr *sa)
{
  if (sa->sa_family == AF_INET)
  {
    return &(((struct sockaddr_in *)sa)->sin_addr);
  }

  return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

void close_network_socket(int *socketfd)
{
  close(*socketfd);
  *socketfd = -1;
}


int create_network_socket(int *socketfd, const char *inetAddr, unsigned int portNum)
{
  //socketfd = -1;
  if (*socketfd > 0)
  {
    print_error_normal_message("Socket currently connected.\n");
    return -1;
  }
  struct sockaddr_in serverinfo;

  /* Create TCP socket */
  if ((*socketfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
  {
    print_error_normal_message("Failed to create TCP socket.\n");
    return -1;
  }

  /* Construct the server sockaddr_in structure */
  memset(&serverinfo, 0, sizeof(serverinfo));
  serverinfo.sin_family = AF_INET;                  /* Internet IP */
  serverinfo.sin_addr.s_addr = inet_addr(inetAddr); /* Server IP address */
  serverinfo.sin_port = htons(portNum);             /* Server Port */

  /* Establish connection */
  if (connect(*socketfd,
              (struct sockaddr *)&serverinfo,
              sizeof(serverinfo)) < 0)
  {
    print_error_normal_message("Failed to connect with server.\n");
    close_network_socket(socketfd);
    return -1;
  }

  return 1;
}

int send_data_to_server(int *socketfd, unsigned char *dataSend)
{

  if (*socketfd < -1)
  {
    print_error_normal_message("Not connecting to server.\n");
    return -1;
  }

  if (send(*socketfd, dataSend, MAXDATASIZE, 0) != MAXDATASIZE)
  {
    print_error_normal_message("Mismatch in number of send data.\n");
    return -1;
  }
  return 1;
}

int get_data_from_server(int *socketfd, unsigned char *dataReceive)
{
  int byteRc = 0;
  int received = 0;

  if (*socketfd < -1)
  {
    print_error_normal_message("Not connecting to server.\n");
    return -1;
  }

  while (received < MAXDATASIZE)
  {
    if ((byteRc = recv(*socketfd, dataReceive, MAXDATASIZE - 1, 0)) < 1)
    {
      print_error_normal_message("Failed to receive from server.\n");
      return -1;
    }
    received += byteRc;
  }
  return 1;
}
