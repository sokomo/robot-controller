/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <common_network.h>
#include <server_rb_backend.h>

#define MAX_CONNECTION 1

void receive_data_client(int socketfd, unsigned char *dataReceive)
{
  int received = -1;
  int index = 0;

  if ((received = recv(socketfd, dataReceive, MAXDATASIZE, 0)) < 0)
    print_error_normal_message("Failed to received initial byte from client.\n");

  while (received > 0)
  {
    /*
    fprintf(stdout, "Data receive:\n");
    for (index = 0; index < MAXDATASIZE; index++)
      fprintf(stdout, "%d ", *(dataReceive + index));
    fprintf(stdout, "\n");
    */
    choose_packet_type(dataReceive);

    if ((received = recv(socketfd, dataReceive, MAXDATASIZE, 0)) < 0)
      fprintf(stdout,"Not received more bytes from client.\n");
  }

  close(socketfd);
}


int main(int argc, char *argv[])
{
  int serversock, clientsock;
  struct sockaddr_in serverinfo, clientinfo;
  unsigned int clientLength = 0;
  unsigned char dataServerControl[MAXDATASIZE];

  if (argc != 2)
  {
    print_error_normal_message("USAGE: server_network <port>\n");
    exit(1);
  }

  /* Create TCP socket */
  if ((serversock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
  {
    print_error_normal_message("Failed to create socket.\n");
  }

  /* Construct server sockaddr_in structure */
  memset(&serverinfo, 0, sizeof(serverinfo));
  serverinfo.sin_family = AF_INET;                /* TCP IP */
  serverinfo.sin_addr.s_addr = htonl(INADDR_ANY); /* Incomming address */
  serverinfo.sin_port = htons(atoi(argv[1]));      /* Server port */

  /* Bind the server socket */
  if (bind(serversock, (struct sockaddr *) &serverinfo, sizeof(serverinfo)) < 0)
  {
    print_error_normal_message("Failed to bind server socket.\n");
  }

  /* Listen on the server socket */
  if (listen(serversock, MAX_CONNECTION) < 0)
  {
    print_error_normal_message("Failed to listen on the server socket.\n");
  }
  

  /* Socket factory */
  while (1)
  {
    clientLength = sizeof(clientinfo);
    /* Waiting for client connection */
    if ((clientsock = accept(serversock, (struct sockaddr *) &clientinfo, &clientLength)) < 0)
    {
      print_error_normal_message("Failed to accept client connection.\n");
    }
    /* fprintf(stdout, "Get here.\n");  */

    fprintf(stdout, "Client connected: %s\n", inet_ntoa(clientinfo.sin_addr));
    receive_data_client(clientsock, dataServerControl);
  }
}
