#ifndef _SERVER_RB_BACKEND_H_
#define _SERVER_RB_BACKEND_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <define_id_motors.h>

void printCommStatus(int CommStatus);
void printErrorCode(void);

int open_serial_device(char* deviceName, int baudNum);
void close_serial_device(void);

/* Network communication  */
void choose_packet_type(unsigned char *packetData);

#ifdef __cplusplus
}
#endif


#endif // _RB_BACKEND_H_
