#ifndef _CLIENT_RB_BACKEND_H_
#define _CLIENT_RB_BACKEND_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <define_id_motors.h>

void printCommStatus(int CommStatus);
void printErrorCode(void);

int open_serial_device();
void close_serial_device(void);
int rotate_motor(int id, int value);
int rotate_multi_motors(int* idList, int numOfId, int* value);
int rotate_multi_motors_same_value(int* idList, int numOfId, int value);
void set_speed(int id, int value);
int get_current_pos(int id);

/* High level communication */
void move_plinth_high_level(int pos);
void move_joints_high_level(int posJoint1, int posJoint2);
void move_neck_high_level(int pos);
void move_tongs_high_level(int pos);
void step_moving_automation(int posPlinth, int posJoint1, int posJoint2, int posTongs);

/* Network communication  */
void choose_command_to_move(int motorid, int pos);

#ifdef __cplusplus
}
#endif


#endif // _RB_BACKEND_H_
