#ifndef _DEFINE_ID_MOTORS_H_
#define _DEFINE_ID_MOTORS_H_


#define MOTOR_PLINTH          1

#define MOTOR_JOINT1_1        2
#define MOTOR_JOINT1_2        3

#define MOTOR_JOINT2_1        4
#define MOTOR_JOINT2_2        5

#define MOTOR_NECK            7
#define MOTOR_TONGS           6


#define MOTOR_GLOBAL_ID       254

#endif
