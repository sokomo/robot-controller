#ifndef _DYNAMIXEL_CLIENT_H_
#define _DYNAMIXEL_CLIENT_H_

#include <dynamixel_common.h>

#ifdef __cplusplus
extern "C" {
#endif

void dxl_send_network(void);
void dxl_receive_network(void);

void dxl_network_open_device(void);
void dxl_network_close_device(void);

void dxl_set_txpacket_id(int id);

void dxl_set_txpacket_instruction(int instruction);

void dxl_set_txpacket_parameter(int index, int value);
void dxl_set_txpacket_length(int length);

int dxl_get_rxpacket_error(int errbit);


int dxl_get_rxpacket_length(void);
int dxl_get_rxpacket_parameter(int index);


/* utility for value */
int dxl_makeword(int lowbyte, int highbyte);
int dxl_get_lowbyte(int word);
int dxl_get_highbyte(int word);


/* packet communication methods */
void dxl_tx_packet(void);
void dxl_rx_packet(void);
void dxl_txrx_packet(void);

int dxl_get_result(void);


/* high communication methods */
void dxl_ping(int id);
int dxl_read_byte(int id, int address);
void dxl_write_byte(int id, int address, int value);
int dxl_read_word(int id, int address);
void dxl_write_word(int id, int address, int value);

/* Network command define  */

#ifdef __cplusplus
}
#endif

#endif
