#ifndef _COMMON_NETWORK_H_
#define _COMMON_NETWORK_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <common_define.h>

#define MAXDATASIZE 160


#endif
