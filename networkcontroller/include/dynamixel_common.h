#ifndef _DYNAMIXEL_COMMON_H_
#define _DYNAMIXEL_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif


#define MAXNUM_TXPARAM		(150)
#define MAXNUM_RXPARAM		(60)

#define BROADCAST_ID		(254)


#define INST_PING			(1)
#define INST_READ			(2)
#define INST_WRITE			(3)
#define INST_REG_WRITE		(4)
#define INST_ACTION			(5)
#define INST_RESET			(6)
#define INST_SYNC_WRITE		(131)


/* Error check */
#define ERRBIT_VOLTAGE		(1)
#define ERRBIT_ANGLE		(2)
#define ERRBIT_OVERHEAT		(4)
#define ERRBIT_RANGE		(8)
#define ERRBIT_CHECKSUM		(16)
#define ERRBIT_OVERLOAD		(32)
#define ERRBIT_INSTRUCTION	(64)


/* Status package */
#define COMM_TXSUCCESS		(0)
#define COMM_RXSUCCESS		(1)
#define COMM_TXFAIL		(2)
#define COMM_RXFAIL		(3)
#define COMM_TXERROR		(4)
#define COMM_RXWAITING		(5)
#define COMM_RXTIMEOUT		(6)
#define COMM_RXCORRUPT		(7)




/* Network command define  */
#define COMMAND_DYNAMIXEL  (1)
#define COMMAND_CONNECT    (1)
#define COMMAND_DISCONNECT (0)
#define COMMAND_PACKAGE    (2)

#ifdef __cplusplus
}
#endif

#endif
