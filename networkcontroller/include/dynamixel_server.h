#ifndef _DYNAMIXEL_SERVER_H_
#define _DYNAMIXEL_SERVER_H_

#include <dynamixel_common.h>

#ifdef __cplusplus
extern "C" {
#endif

/* device control methods */
int dxl_initialize(char* deviceName, int baudnum);
void dxl_terminate();


void dxl_set_txpacket_instruction(int instruction);

int dxl_get_rxpacket_error(int errbit);

/* utility for value */
int dxl_makeword(int lowbyte, int highbyte);
int dxl_get_lowbyte(int word);
int dxl_get_highbyte(int word);


/* packet communication methods */
void dxl_tx_packet(unsigned char *gbInstructionPacket);
void dxl_rx_packet(unsigned char *gbInstructionPacket);
void dxl_txrx_packet(unsigned char *gbInstructionPacket);

int dxl_get_result(void);


#ifdef __cplusplus
}
#endif

#endif
