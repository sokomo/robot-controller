/* Author: Duc Tran
   License: GPLv3
 */

#ifndef _COLOR_DEFINE_H_
#define _COLOR_DEFINE_H_


/* ******************************************* */
/* All color text under here are all bold mode
   For more information, reference at:
   http://en.wikipedia.org/wiki/ANSI_escape_code

   Only support for terminal linux
 */


#define ANSI_COLOR_RED     "\x1b[31;1m"
#define ANSI_COLOR_GREEN   "\x1b[32;1m"
#define ANSI_COLOR_YELLOW  "\x1b[33;1m"
#define ANSI_COLOR_BLUE    "\x1b[34;1m"
#define ANSI_COLOR_MAGENTA "\x1b[35;1m"
#define ANSI_COLOR_CYAN    "\x1b[36;1m"
#define ANSI_COLOR_RESET   "\x1b[0m"



#endif // _COLOR_DEFINE_H_
