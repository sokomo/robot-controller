#ifndef _CLIENT_NETWORK_H_
#define _CLIENT_NETWORK_H_

#include "common_network.h"

int socketfd;

int create_network_socket(int *socketfd, const char *inetAddr, unsigned int portNum);
int send_data_to_server(int *socketfd,  unsigned char *dataSend);
int get_data_from_server(int *socketfd, unsigned char *dataReceive);
void close_network_socket(int *socketfd);

#endif
