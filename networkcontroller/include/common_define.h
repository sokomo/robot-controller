#ifndef _COMMON_DEFINE_H
#define _COMMON_DEFINE_H

#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

extern int errno;

void print_errno_message();
void print_error_normal_message(char *err_message);

#endif
