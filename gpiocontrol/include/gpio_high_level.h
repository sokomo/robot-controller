#ifndef _GPIO_HIGH_LEVEL_
#define _GPIO_HIGH_LEVEL_

#ifdef __cplusplus
extern "C"
{
#endif

#define DIR_IN      0
#define DIR_OUT   1



int open_gpio_list();
int close_gpio_list();
/* int get_button_value(int port); */
int write_output_value(int port, int value);
int get_port_value(int port);


#ifdef __cplusplus
}
#endif

#endif



