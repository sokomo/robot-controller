#ifndef GPIO_CONTROL_H
#define GPIO_CONTROL_H
#include "timer.h"
#include <QMainWindow>

namespace Ui {
class gpio_control;
}

class gpio_control : public QMainWindow
{
    Q_OBJECT

public:
    explicit gpio_control(QWidget *parent = 0);
    ~gpio_control();
    Timer *timerThread;
    QPixmap image1 ;
     QPixmap image2 ;
     QIcon ButtonIconOn;
     QIcon ButtonIconOff;

public slots:
     void currentTime(int position);
    void getTime();
    void showTime();

private slots:
    void on_btnLight1_clicked();

    void on_btnLight2_clicked();

    void on_btnLight3_clicked();

    void on_btnLight4_clicked();

    void on_btnLight5_clicked();

    void on_btnLight6_clicked();

    void on_btnLight7_clicked();

    void on_btnLight8_clicked();

    void timer();

    void on_btnSet_clicked();

    void on_btnEnable_clicked();


private:
    void delay();

private:
    Ui::gpio_control *ui;
};

#endif // GPIO_CONTROL_H
