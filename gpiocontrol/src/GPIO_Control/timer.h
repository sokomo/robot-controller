#ifndef TIMER_H
#define TIMER_H

//#include "gpio_control.h"
#include <QThread>


class Timer : public QThread
{
    Q_OBJECT
public:
    explicit Timer(QObject *parent = 0);
    void run();
    bool Stop;

signals:
    void signal(int);


public slots:

};

#endif // TIMER_H
