#-------------------------------------------------
#
# Project created by QtCreator 2013-11-20T00:33:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GPIO_Control
TEMPLATE = app


SOURCES += main.cpp\
        gpio_control.cpp \
    timer.cpp

HEADERS  += gpio_control.h \
    timer.h

FORMS    += gpio_control.ui

INCLUDEPATH += ../../include

LIBS += -L../lib -lgpio


RESOURCES += \
    image.qrc
