#include "gpio_control.h"
#include "ui_gpio_control.h"
//#include "gpio.h"
#include "gpio_high_level.h"
#include <QDateTime>
#include <QtCore/QString>
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QTextStream>
#include <QTimer>

//QString imageOn = "./img/1.png";
//QString imageOff = "./img/2.png";
 bool Enable ;
 bool changeTime ;
 int test ;
 int status;
 int test2;
int status2;
//void    QWidget::setWindowTitle ( "Device Controller" );

gpio_control::gpio_control(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::gpio_control)
{



    ui->setupUi(this);
    this->setWindowTitle("Device Controller");
    //connect(ui->exit,SIGNAL(clicked()),QCoreApplication::instance(), SLOT(exit()));
    timerThread = new Timer(this);
    connect(timerThread,SIGNAL(signal(int)),this,SLOT(currentTime(int)));

    image1 = QPixmap("./img/1.png");
    image2 = QPixmap("./img/2.png");
    ButtonIconOn = QIcon(":/img/img/2.PNG");
    ButtonIconOff = QIcon(":/img/img/1.PNG");

    open_gpio_list();

    int port1,port2,port3,port4,port5,port6,port7,port8;
    port1 = get_port_value(0);
    port2 = get_port_value(1);
    port3 = get_port_value(2);
    port4 = get_port_value(3);
    port5 = get_port_value(4);
    port6 = get_port_value(5);
    port7 = get_port_value(6);
    port8 = get_port_value(7);
    if(port1 == 1)
    {
    ui->btnLight1->setIcon(ButtonIconOff);
    ui->btnLight1->setIconSize(QSize(120,120));
    }
    if(port1 == 0)
    {
    ui->btnLight1->setIcon(ButtonIconOn);
    ui->btnLight1->setIconSize(QSize(120,120));
    }
    if(port2 == 1)
    {
    ui->btnLight2->setIcon(ButtonIconOff);
    ui->btnLight2->setIconSize(QSize(120,120));
    }
    if(port2 == 0)
    {
    ui->btnLight2->setIcon(ButtonIconOn);
    ui->btnLight2->setIconSize(QSize(120,120));
    }
    if(port3 == 1)
    {
    ui->btnLight3->setIcon(ButtonIconOff);
    ui->btnLight3->setIconSize(QSize(120,120));
    }
    if(port3 == 0)
    {
    ui->btnLight3->setIcon(ButtonIconOn);
    ui->btnLight3->setIconSize(QSize(120,120));
    }
    if(port4 == 1)
    {
    ui->btnLight4->setIcon(ButtonIconOff);
    ui->btnLight4->setIconSize(QSize(120,120));
    }
    if(port4 == 0)
    {
    ui->btnLight4->setIcon(ButtonIconOn);
    ui->btnLight4->setIconSize(QSize(120,120));
    }
    if(port5 == 1)
    {
    ui->btnLight5->setIcon(ButtonIconOff);
    ui->btnLight5->setIconSize(QSize(120,120));
    }
    if(port5 == 0)
    {
    ui->btnLight5->setIcon(ButtonIconOn);
    ui->btnLight5->setIconSize(QSize(120,120));
    }
    if(port6 == 1)
    {
    ui->btnLight6->setIcon(ButtonIconOff);
    ui->btnLight6->setIconSize(QSize(120,120));
    }
    if(port6 == 0)
    {
    ui->btnLight6->setIcon(ButtonIconOn);
    ui->btnLight6->setIconSize(QSize(120,120));
    }
    if(port7 == 1)
    {
    ui->btnLight7->setIcon(ButtonIconOff);
    ui->btnLight7->setIconSize(QSize(120,120));
    }
    if(port7 == 0)
    {
    ui->btnLight7->setIcon(ButtonIconOn);
    ui->btnLight7->setIconSize(QSize(120,120));
    }
    if(port8 == 1)
    {
    ui->btnLight8->setIcon(ButtonIconOff);
    ui->btnLight8->setIconSize(QSize(120,120));
    }
    if(port8 == 0)
    {
    ui->btnLight8->setIcon(ButtonIconOn);
    ui->btnLight8->setIconSize(QSize(120,120));
    }

    Enable = 1;
    changeTime = 0;
    test=1;
    status=0;
    test2=1;
    status2=0;
    ui->btnEnable->setText("Enable");
    ui->timeEdit->setDisabled(1);
    ui->timeEdit2->setDisabled(1);
    ui->timeEdit3->setDisabled(1);
    ui->timeEdit4->setDisabled(1);
    ui->timeEdit5->setDisabled(1);
    ui->timeEdit6->setDisabled(1);
    ui->timeEdit7->setDisabled(1);
    ui->timeEdit8->setDisabled(1);
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(showTime()));
    timer->start(1000);
    showTime();


}

gpio_control::~gpio_control()
{
    delete ui;
    close_gpio_list();
}

void gpio_control::showTime()
{
    QTime time = QTime::currentTime();
    QString text = time.toString("hh:mm:ss");
    //if ((time.second() % 2) == 0)
        //text[3] = ' ';
    ui->lblTime->setText(text);
}


void gpio_control::getTime()
{
    QString time1 = ui->timeEdit->dateTime().toString(ui->timeEdit->displayFormat());
    QStringList list1 = time1.split(":");
    QString shour1 = list1.value(0);
    QString smin1 = list1.value(1);
    QString ssec1 = list1.value(2);


    QString time2 = ui->timeEdit2->dateTime().toString(ui->timeEdit2->displayFormat());
    QStringList list2 = time2.split(":");
    QString shour2 = list2.value(0);
    QString smin2 = list2.value(1);
    QString ssec2 = list2.value(2);


    QString time3 = ui->timeEdit3->dateTime().toString(ui->timeEdit3->displayFormat());
    QStringList list3 = time3.split(":");
    QString shour3 = list3.value(0);
    QString smin3 = list3.value(1);
    QString ssec3 = list3.value(2);


    QString time4 = ui->timeEdit4->dateTime().toString(ui->timeEdit4->displayFormat());
    QStringList list4 = time4.split(":");
    QString shour4 = list4.value(0);
    QString smin4 = list4.value(1);
    QString ssec4 = list4.value(2);


    QString time5 = ui->timeEdit5->dateTime().toString(ui->timeEdit5->displayFormat());
    QStringList list5 = time5.split(":");
    QString shour5 = list5.value(0);
    QString smin5 = list5.value(1);
    QString ssec5 = list5.value(2);


    QString time6 = ui->timeEdit6->dateTime().toString(ui->timeEdit6->displayFormat());
    QStringList list6 = time6.split(":");
    QString shour6 = list6.value(0);
    QString smin6 = list6.value(1);
    QString ssec6 = list6.value(2);


    QString time7 = ui->timeEdit7->dateTime().toString(ui->timeEdit7->displayFormat());
    QStringList list7 = time7.split(":");
    QString shour7 = list7.value(0);
    QString smin7 = list7.value(1);
    QString ssec7 = list7.value(2);


    QString time8 = ui->timeEdit8->dateTime().toString(ui->timeEdit8->displayFormat());
    QStringList list8 = time8.split(":");
    QString shour8 = list8.value(0);
    QString smin8 = list8.value(1);
    QString ssec8 = list8.value(2);



    QFile file("/text.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    out << shour1 + " " + smin1 + " " + ssec1  +
           shour2 + " " + smin2 + " " + ssec2  +
           shour3 + " " + smin3 + " " + ssec3  +
           shour4 + " " + smin4 + " " + ssec4  +
           shour5 + " " + smin5 + " " + ssec5  +
           shour6 + " " + smin6 + " " + ssec6  +
           shour7 + " " + smin7 + " " + ssec7  +
           shour8 + " " + smin8 + " " + ssec8 ;

    // optional, as QFile destructor will already do it:
    file.close();

}

void gpio_control::currentTime(int position)
{




    //int a= position;


    if((position % 10) == 1)

        {
            int n = 0;
            n = get_port_value(0);

            if (n == 1)
            {
                //test=1;
                ui->btnLight1->setIcon(ButtonIconOn);
                ui->btnLight1->setIconSize(QSize(120,120));
                //status=1;
                write_output_value(0,0);
            }
            else if (n == 0)
            {
                //test=0;
                ui->btnLight1->setIcon(ButtonIconOff);
                ui->btnLight1->setIconSize(QSize(120,120));
                write_output_value(0,1);
                //status=0;
            }

        }
       if (((position/10) % 10) == 1)

        {
            int n = 0;
            n = get_port_value(1);

            if (n == 1)
            {
                //test2=1;
                ui->btnLight2->setIcon(ButtonIconOn);
                ui->btnLight2->setIconSize(QSize(120,120));
                //status2=1;
                write_output_value(1,0);
            }
            else if (n == 0)
            {
                //test2=0;
                ui->btnLight2->setIcon(ButtonIconOff);
                ui->btnLight2->setIconSize(QSize(120,120));
                //status2=0;
                write_output_value(1,1);
            }
        }
        if (((position/100) % 10) == 1)
        {
            int n = 0;
            n = get_port_value(2);

            if (n == 1)
            {

                ui->btnLight3->setIcon(ButtonIconOn);
                ui->btnLight3->setIconSize(QSize(120,120));
                write_output_value(2,0);
            }
            else if (n == 0)
            {

                ui->btnLight3->setIcon(ButtonIconOff);
                ui->btnLight3->setIconSize(QSize(120,120));
                write_output_value(2,1);
            }
        }
        if (((position/1000) % 10) == 1)
        {
            int n = 0;
            n = get_port_value(3);

            if (n == 1)
            {

                ui->btnLight4->setIcon(ButtonIconOn);
                ui->btnLight4->setIconSize(QSize(120,120));
                write_output_value(3,0);
            }
            else if (n == 0)
            {

                ui->btnLight4->setIcon(ButtonIconOff);
                ui->btnLight4->setIconSize(QSize(120,120));
                write_output_value(3,1);
            }
        }
        if (((position/10000) % 10) == 1)
        {
            int n = 0;
            n = get_port_value(4);

            if (n == 1)
            {

                ui->btnLight5->setIcon(ButtonIconOn);
                ui->btnLight5->setIconSize(QSize(120,120));
                write_output_value(4,0);
            }
            else if (n == 0)
            {

                ui->btnLight5->setIcon(ButtonIconOff);
                ui->btnLight5->setIconSize(QSize(120,120));
                write_output_value(4,1);
            }
        }
        if (((position/100000) % 10) == 1)
        {
            int n = 0;
            n = get_port_value(5);

            if (n == 1)
            {

                ui->btnLight6->setIcon(ButtonIconOn);
                ui->btnLight6->setIconSize(QSize(120,120));
                write_output_value(5,0);
            }
            else if (n == 0)
            {

                ui->btnLight6->setIcon(ButtonIconOff);
                ui->btnLight6->setIconSize(QSize(120,120));
                write_output_value(5,1);
            }
        }
        if (((position/1000000) % 10) == 1)
        {
            int n = 0;
            n = get_port_value(6);

            if (n == 1)
            {

                ui->btnLight7->setIcon(ButtonIconOn);
                ui->btnLight7->setIconSize(QSize(120,120));
                write_output_value(6,0);
            }
            else if (n == 0)
            {

                ui->btnLight7->setIcon(ButtonIconOff);
                ui->btnLight7->setIconSize(QSize(120,120));
                write_output_value(6,1);
            }
        }
        if (((position/10000000) % 10) == 1)
        {
            int n = 0;
            n = get_port_value(7);

            if (n == 1)
            {

                ui->btnLight8->setIcon(ButtonIconOn);
                ui->btnLight8->setIconSize(QSize(120,120));
                write_output_value(7,0);
            }
            else if (n == 0)
            {

                ui->btnLight8->setIcon(ButtonIconOff);
                ui->btnLight8->setIconSize(QSize(120,120));
                write_output_value(7,1);
            }
        }


    //delay();

}

void gpio_control::timer()
{

    int a=0;
    //QTimer *timer = new QTimer(this);
    //connect(timer, SIGNAL(timeout()), this, SLOT(showTime()));
    //timer->start(1200);
    //get current time

}

void gpio_control::delay()
{
    QTime dieTime= QTime::currentTime().addMSecs(50);
    while( QTime::currentTime() < dieTime )
    QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}


void gpio_control::on_btnLight1_clicked()
{


    int a = 1;
    QString b = QString::number(a);

    //ui->label->setText("Time: " + b +"   " + timeString);

    int n = 0;
    n = get_port_value(0);

    if (n == 1)
    {
    //n=1;
        ui->btnLight1->setIcon(ButtonIconOn);
        ui->btnLight1->setIconSize(QSize(120,120));
        write_output_value(0,0);
    }
    else if (n == 0)
    {
        //n=0;
        ui->btnLight1->setIcon(ButtonIconOff);
        ui->btnLight1->setIconSize(QSize(120,120));
        write_output_value(0,1);
    }
}


void gpio_control::on_btnLight2_clicked()
{

    int n = 0;
    n = get_port_value(1);

    if (n == 1)
    {
    //n=1;
        ui->btnLight2->setIcon(ButtonIconOn);
        ui->btnLight2->setIconSize(QSize(120,120));
        write_output_value(1,0);
    }
    else if (n == 0)
    {
        //n=0;
        ui->btnLight2->setIcon(ButtonIconOff);
        ui->btnLight2->setIconSize(QSize(120,120));
        write_output_value(1,1);
    }
}

void gpio_control::on_btnLight3_clicked()
{


    int n = 0;
    n = get_port_value(2);

    if (n == 1)
    {
    //n=1;
        ui->btnLight3->setIcon(ButtonIconOn);
        ui->btnLight3->setIconSize(QSize(120,120));
        write_output_value(2,0);
    }
    else if (n == 0)
    {
        //n=0;
        ui->btnLight3->setIcon(ButtonIconOff);
        ui->btnLight3->setIconSize(QSize(120,120));
        write_output_value(2,1);
    }
}

void gpio_control::on_btnLight4_clicked()
{


    int n = 0;
    n = get_port_value(3);

    if (n == 1)
    {
    //n=1;
        ui->btnLight4->setIcon(ButtonIconOn);
        ui->btnLight4->setIconSize(QSize(120,120));
        write_output_value(3,0);
    }
    else if (n == 0)
    {
        //n=0;
        ui->btnLight4->setIcon(ButtonIconOff);
        ui->btnLight4->setIconSize(QSize(120,120));
        write_output_value(3,1);
    }
}

void gpio_control::on_btnLight5_clicked()
{


    int n = 0;
    n = get_port_value(4);

    if (n == 1)
    {
    //n=1;
        ui->btnLight5->setIcon(ButtonIconOn);
        ui->btnLight5->setIconSize(QSize(120,120));
        write_output_value(4,0);
    }
    else if (n == 0)
    {
      //  n=0;
        ui->btnLight5->setIcon(ButtonIconOff);
        ui->btnLight5->setIconSize(QSize(120,120));
        write_output_value(4,1);
    }
}

void gpio_control::on_btnLight6_clicked()
{


    int n = 0;
    n = get_port_value(5);

    if (n == 1)
    {
    //n=1;
        ui->btnLight6->setIcon(ButtonIconOn);
        ui->btnLight6->setIconSize(QSize(120,120));
        write_output_value(5,0);
    }
    else if (n == 0)
    {
        //n=0;
        ui->btnLight6->setIcon(ButtonIconOff);
        ui->btnLight6->setIconSize(QSize(120,120));
        write_output_value(5,1);
    }
}

void gpio_control::on_btnLight7_clicked()
{


    int n = 0;
    n = get_port_value(6);

    if (n == 1)
    {
    //n=1;
        ui->btnLight7->setIcon(ButtonIconOn);
        ui->btnLight7->setIconSize(QSize(120,120));
        write_output_value(6,0);
    }
    else if (n == 0)
    {
        //n=0;
        ui->btnLight7->setIcon(ButtonIconOff);
        ui->btnLight7->setIconSize(QSize(120,120));
        write_output_value(6,1);
    }
}

void gpio_control::on_btnLight8_clicked()
{


    int n = 0;
    n = get_port_value(7);

    if (n == 1)
    {
    //n=1;
        ui->btnLight8->setIcon(ButtonIconOn);
        ui->btnLight8->setIconSize(QSize(120,120));
        write_output_value(7,0);
    }
    else if (n == 0)
    {
        //n=0;
        ui->btnLight8->setIcon(ButtonIconOff);
        ui->btnLight8->setIconSize(QSize(120,120));
        write_output_value(7,1);
    }
}

void gpio_control::on_btnSet_clicked()
{

    //QString abc = ui->timeEdit->dateTime().toString(ui->timeEdit->displayFormat());
    //ui->btnSet->setText(abc);
    getTime();

    if(!Enable && changeTime == 0 )
    {
        ui->btnSet->setText("Change Time");
        changeTime =1;
        getTime();
        ui->timeEdit->setDisabled(1);
        ui->timeEdit2->setDisabled(1);
        ui->timeEdit3->setDisabled(1);
        ui->timeEdit4->setDisabled(1);
        ui->timeEdit5->setDisabled(1);
        ui->timeEdit6->setDisabled(1);
        ui->timeEdit7->setDisabled(1);
        ui->timeEdit8->setDisabled(1);


        //if(status==0){ test=0;}
        //else test=1;
        //if(status2==0){ test2=0;}
        //else test2=1;

        //test2=1-test2;
        timerThread = new Timer(this);
        connect(timerThread,SIGNAL(signal(int)),this,SLOT(currentTime(int)));
        timerThread->start();
    }
    else if(!Enable && changeTime ==1)
    {
        ui->btnSet->setText("Set Time");
        changeTime = 0;

        ui->timeEdit->setDisabled(0);
        ui->timeEdit2->setDisabled(0);
        ui->timeEdit3->setDisabled(0);
        ui->timeEdit4->setDisabled(0);
        ui->timeEdit5->setDisabled(0);
        ui->timeEdit6->setDisabled(0);
        ui->timeEdit7->setDisabled(0);
        ui->timeEdit8->setDisabled(0);
        timerThread->exit();
    }
    else if(Enable)
    {
        timerThread->Stop;
    }
}

void gpio_control::on_btnEnable_clicked()
{
    if (Enable)
    {
        ui->btnEnable->setText("Disable");
        ui->btnSet->setText("Set Time");
        ui->timeEdit->setDisabled(0);
        ui->timeEdit2->setDisabled(0);
        ui->timeEdit3->setDisabled(0);
        ui->timeEdit4->setDisabled(0);
        ui->timeEdit5->setDisabled(0);
        ui->timeEdit6->setDisabled(0);
        ui->timeEdit7->setDisabled(0);
        ui->timeEdit8->setDisabled(0);
        changeTime = 0;
        Enable = 0;
    }
    else
    {
        ui->btnEnable->setText("Enable");
        ui->timeEdit->setDisabled(1);
        ui->timeEdit2->setDisabled(1);
        ui->timeEdit3->setDisabled(1);
        ui->timeEdit4->setDisabled(1);
        ui->timeEdit5->setDisabled(1);
        ui->timeEdit6->setDisabled(1);
        ui->timeEdit7->setDisabled(1);
        ui->timeEdit8->setDisabled(1);
        changeTime = 0;
        Enable = 1;
    }

}
