/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "gpio.h"
#include <string.h>
#include <stdio.h>

#define DIR_IN      0
#define DIR_OUT   1

#define GPIO0_OFFSET       0
#define GPIO1_OFFSET       32
#define GPIO2_OFFSET       64
#define GPIO3_OFFSET       96


int g_gpioOutputList[] = { GPIO1_OFFSET + 30,
                           GPIO1_OFFSET + 31,
                           GPIO1_OFFSET + 4,
                           GPIO1_OFFSET + 5,
                           GPIO1_OFFSET + 0,
                           GPIO1_OFFSET + 1,
                           GPIO1_OFFSET + 6,
                           GPIO1_OFFSET + 7,
                           GPIO1_OFFSET + 2,
                           GPIO1_OFFSET + 3
};
/*
int g_gpioInputList[] = { GPIO0_OFFSET + 7,
                          GPIO1_OFFSET + 28,
                          GPIO1_OFFSET + 16,
                          GPIO1_OFFSET + 19,
                          GPIO3_OFFSET + 19
};
*/

int open_gpio_output_list()
{
  int index = 0;
  int sizeList = sizeof(g_gpioOutputList) / sizeof(int);
  printf("Size list: %d\n", sizeList);
  for (index = 0; index < sizeList; index++)
  {
    /* printf("%d ", *(g_gpioOutputList + index)); */
    if (gpio_open(*(g_gpioOutputList + index), DIR_OUT) < 0)
      return 0;
  }
  return 1;
}

/*
int open_gpio_input_list()
{
    int index = 0;
    int sizeList = sizeof(g_gpioInputList) / sizeof(int);
    for (index = 0; index < sizeList; index++)
    {
        if (gpio_open(*(g_gpioOutputList + index), DIR_IN) < 0)
            return 0;
    }
    return 1;
}
*/

int close_gpio_output_list()
{
  int index = 0;
  int sizeList = sizeof(g_gpioOutputList);
  for (index = 0; index < sizeList; index++)
  {
    gpio_close(*(g_gpioOutputList + index));
  }

  return 1;
}

/*
int close_gpio_input_list()
{
    int index = 0;
    int sizeList = sizeof(g_gpioInputList);
    for (index = 0; index < sizeList; index++)
    {
        gpio_close(*(g_gpioInputList + index));
    }

    return 1;
}
*/

int open_gpio_list()
{
  open_gpio_output_list();
  /* open_gpio_input_list(); */

  return 1;
}

int close_gpio_list()
{    
  close_gpio_output_list();
  /* close_gpio_input_list(); */
  return 1;
}

/*
int get_button_value(int port)
{
  return gpio_read(*(g_gpioInputList + port));
}
*/

int get_port_value(int port)
{
  return gpio_read(*(g_gpioOutputList + port));
}

int write_output_value(int port, int value)
{
  return gpio_write(*(g_gpioOutputList + port), value);
}
