/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "gpio.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h> /* Manipulate GPIO */
#include <fcntl.h> /* Define declaration read write file  */

#include <errno.h>
#include <string.h>


extern int errno;


void print_error_message()
{
  fprintf(stderr, "Error is %s (errno=%d)\n",
          strerror(errno), errno);
}


int gpio_pin_exists(int port)
{
  int pinExists[] = {135,136,137,138,139,143,144,145,146};
  int index = 0;
  int sizeArray = sizeof(pinExists) / sizeof(int);

  for(index = 0; index < sizeArray; index++)
  {
    if(port == pinExists[index])
      return 1;
  }
  return 0;
}

int gpio_open(int port, int DDR)
{
  int fd = -1;             /* Use for open file device  */
  int fresult = -1;      /* Use for result write */
  char number[4];
  char file[35],ris[5];

  if(gpio_pin_exists(port) == 1)
  {
    fprintf(stdout, "Exist.\n");
    return -1;
  }
  //fprintf(stdout, "%s\n", EXPORT);
  fd  =  open(EXPORT, O_WRONLY); /* Open Write Only */
    
  if (fd < 0)
  {
    print_error_message();
    return -2;
  }

  sprintf(number, "%d\n", port);
  fresult = write(fd, number, sizeof(number));
  if (fresult < 0)
  {
    close(fd);
    print_error_message();
    return -2;
  }
  fresult = -1;
  close(fd);

  sprintf(file, "/sys/class/gpio/gpio%d/direction", port);
  fd  =  open(file, O_WRONLY);
  if (fd < 0)
  {
    print_error_message();
    return -2;
  }

  if (!DDR)
    sprintf(ris,IN);
  else
    sprintf(ris,OUT);

  fresult = write(fd, ris, sizeof(ris));
  if (fresult < 0)
  {
    close(fd);
    print_error_message();
    return -2;
  }
  close(fd);
    
  return 0;
}

int gpio_close(int port)
{
  int fd = -1;
  int fresult = -1;
  char number[4];

  if(gpio_pin_exists(port) == 1)
    return -1;

  fd = open(UNEXPORT, O_WRONLY);
  if (fd < 0)
  {
    print_error_message();
    return -2;
  }

  sprintf(number, "%d\n", port);
  fresult = write(fd, number, sizeof(number));
  if (fresult < 0)
  {
    close(fd);
    print_error_message();
    return -2;
  }

  close(fd);
  return 0;
}

int gpio_read(int port)
{
  int fd = -1;
  int fresult = -1;
  int numResult = 0;
  char number[4];
  char file[31];

  if(gpio_pin_exists(port) == 1)
    return -1;

  sprintf(file, "/sys/class/gpio/gpio%d/value", port);
  fd = open(file, O_RDONLY);
  if (fd < 0)
  {
    print_error_message();
    return -2;
  }

  fresult = read(fd, number, sizeof(number));
  if (fresult < 0)
  {
    close(fd);
    print_error_message();
    return -2;
  }

  sscanf(number, "%d", &numResult);
  close(fd);
  return numResult;
}

int gpio_write(int port, int value)
{
  int fresult = -1;
  int fd = -1;
  char file[35],ris[5];

  if (gpio_pin_exists(port) == 1)
    return -1;

  /* Open device file */
  sprintf(file, "/sys/class/gpio/gpio%d/value", port);
  fd = open(file, O_WRONLY);
  if (fd < 0)
  {
    print_error_message();
    return -2;
  }

  if (!value)
    sprintf(ris,LOW);
  else
    sprintf(ris,HIGH);

  fresult = write(fd, ris, sizeof(ris));

  if (fresult < 0)
  {
    close(fd);
    print_error_message();
    return -2;
  }

  close(fd);

  return 0;
}
