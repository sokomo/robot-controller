#ifndef _CAMERA_LOW_LEVEL_H_
#define _CAMERA_LOW_LEVEL_H_

#include <capture_api.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cpluscplus



int init_capture_device(char *deviceName, unsigned char deviceWrapMode, enum capture_method captureMethod);
void init_format_capture(__u32 width, __u32 height);
void uninit_capture_device();
int start_capture(unsigned long int bufferSize);
void stop_capture();
int capture_frame(unsigned char* destData, __u32 *lengthData);


#ifdef __cplusplus
}
#endif // __cpluscplus


#endif // _CAMERA_LOW_LEVEL_H_
