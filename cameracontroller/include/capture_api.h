
#ifndef _CAPTURE_API_H_
#define _CAPTURE_API_H_

#include <linux/types.h>

#include <linux/videodev2.h>
#include <libv4l2.h>

#ifdef __cplusplus
extern "C" {
#endif

enum capture_method {
    methodUser = 1,
    methodMmap = 0
};

/* Define struct */
struct buffer {
        void   *start;
        size_t  length;
};

int open_capture_device(char *device, unsigned char deviceOpenMode, enum capture_method captureMethod);
void close_capture_device();

void print_errno_message();
void print_error_normal_message(char *err_message);
enum capture_method get_capture_method();

int ioctl_capture(unsigned long int cmd, void *arg);
int ioctl_exists_capture(unsigned long int cmd, void *arg);
int read_capture(void *capBuffer, int dataSize);
void *mmap_capture(size_t lengthData, int64_t offsetData);
int munmap_capture(void *startBuffer, size_t lengthData);

/* User pointer map */
int reqbufs_user(struct v4l2_requestbuffers *reqbuf, __u32 bufType, int countBuf);
int deqbuf_user(struct v4l2_buffer *bufCapture, __u32 bufType, unsigned char *flagAgain);
int qbuf_user(int indexBuf, __u32 bufType, void *bufData, int lengthData);


/* Memory Map */
int reqbufs_mmap(struct v4l2_requestbuffers *reqbuf, __u32 bufType, int countBuf);
int deqbuf_mmap(struct v4l2_buffer *bufCapture, __u32 bufType, unsigned char *flagAgain);
int qbuf_mmap(int indexBuf, __u32 bufType);

int streamon_capture(__u32 bufType);
int streamoff_capture(__u32 bufType);

int get_format_capture(struct v4l2_format *fmt);
int set_format_capture(struct v4l2_format *fmt);

int get_capture_interval(struct v4l2_fract *capInterval);
int set_capture_interval(struct v4l2_fract *capInterval);


#ifdef __cplusplus
}
#endif

#endif // _CAPTURE_API_H_
