#include "colordetector.h"
#include "ui_colordetector.h"
#include <QFile>
#include <QFileDialog>
#include <QImage>
#include <QRect>
#include <QColor>
#include <QPainter>
#include <QDebug>
#include <libv4l2.h>
#include <QTime>
#include <QMouseEvent>
#include <QTransform>
#include <QPalette>
#include "imagelabel.h"
#include <QStandardItemModel>
#include <QMessageBox>
#include <QAbstractItemModel>

#include <iostream>

#include <time.h>


ColorDetector::ColorDetector(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ColorDetector)
{
    ui->setupUi(this);

    initial_values();


    QObject::connect(ui->lbCaptureImage, SIGNAL(mousePressed()),
                     this, SLOT(setPosColor()), Qt::AutoConnection);


    char deviceCamera[20] = "/dev/video0";
    char deviceRobotics[20] = "/dev/ttyUSB0";

    robotics = new ThreadRobotics(0, deviceRobotics, 57600);
    captureDevice = new CaptureDevice(deviceCamera, 1);


    robotics->lbGesture = ui->lbGesture;
    robotics->tableUI = ui->tableView;
    robotics->lbwidth = ui->lbWidth;

    connect(robotics, SIGNAL(updateUITable(QStandardItemModel *)),
            this, SLOT(updateUIRobotics(QStandardItemModel *)));

    connect(captureDevice, SIGNAL(updateUICapture(QImage*)),
            this, SLOT(updateUICapture(QImage*)));

    connect(captureDevice, SIGNAL(updateUIGesture(QString)),
            this, SLOT(updateUIGesture(QString)));

    connect(captureDevice, SIGNAL(updateUISnapshot(QImage*)),
            this, SLOT(updateUISnapshot(QImage*)));

    connect(captureDevice, SIGNAL(updateUIXWidth(int,int)),
            this, SLOT(updateUIXWidth(int,int)));

    ui->tabWidget->setTabEnabled(1, false);

    flagStart = 0;
}

void ColorDetector::initial_values()
{
    QImage boxImage1;
    boxImage1.load("Images/imageBox1.jpg");
    boxImage1 = boxImage1.scaled(QSize(51,81));
    ui->lbbox1->setPixmap(QPixmap::fromImage(boxImage1));

    ui->lbbox2->setStyleSheet("background-color: hsv("+ QString::number(9) + "," + QString::number(181)+","+QString::number(245) +")");

    QImage boxImage3;
    boxImage3.load("Images/imageBox3.jpg");
    boxImage3 = boxImage3.scaled(QSize(51,81));
    ui->lbbox3->setPixmap(QPixmap::fromImage(boxImage3));

    model = new QStandardItemModel(0,0,this);
    ui->tableView->setModel(model);

}

ColorDetector::~ColorDetector()
{
    delete ui;

    delete captureDevice;
    robotics->terminate();
    robotics->close_robotics_device();
    delete robotics;
    robotics = NULL;
    captureDevice = NULL;
}

void ColorDetector::on_rdbBox1_clicked()
{
    captureDevice->flagBox = 1;
    ui->rdbBox1->setChecked(true);
    ui->rdbBox2->setChecked(false);
    ui->rdbBox3->setChecked(false);
    ui->rdbBox4->setChecked(false);
}

void ColorDetector::on_rdbBox2_clicked()
{
    captureDevice->flagBox = 2;
    ui->rdbBox1->setChecked(false);
    ui->rdbBox2->setChecked(true);
    ui->rdbBox3->setChecked(false);
    ui->rdbBox4->setChecked(false);
}
void ColorDetector::on_rdbBox3_clicked()
{
    captureDevice->flagBox = 3;
    ui->rdbBox1->setChecked(false);
    ui->rdbBox2->setChecked(false);
    ui->rdbBox3->setChecked(true);
    ui->rdbBox4->setChecked(false);
}
void ColorDetector::on_rdbBox4_clicked()
{
    captureDevice->flagBox = 4;
    ui->rdbBox1->setChecked(false);
    ui->rdbBox2->setChecked(false);
    ui->rdbBox3->setChecked(false);
    ui->rdbBox4->setChecked(true);
}

void ColorDetector::on_btnStart_clicked()
{
    if (!captureDevice->get_startCapture())
        captureDevice->begin_capture();

    captureDevice->start();
}


void ColorDetector::delay()
{
    QTime dieTime= QTime::currentTime().addMSecs(50);
    while( QTime::currentTime() < dieTime )
    QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}


void ColorDetector::updateUIRobotics(QStandardItemModel *newModel)
{
    QAbstractItemModel *oldModel;
    oldModel = ui->tableView->model();

    ui->tableView->setModel(newModel);

    if (oldModel != NULL)
    {
        delete oldModel;
        oldModel = NULL;
    }
    ui->tableView->update();

}

void ColorDetector::updateUISnapshot(QImage *imgSnapshot)
{
    ui->lbCaptureImage->setPixmap(QPixmap::fromImage(*imgSnapshot));
    ui->lbCaptureImage->update();
}

void ColorDetector::updateUICapture(QImage *frameCapture)
{
    ui->lbColorDetect->setPixmap(QPixmap::fromImage(*frameCapture));
    ui->lbColorDetect->update();
}

void ColorDetector::updateUIGesture(QString strGesture)
{
    ui->lbGesture->setText(strGesture);
    ui->lbGesture->update();
}

void ColorDetector::updateUIXWidth(int posX, int width)
{
    ui->lbPosX->setText(QString::number(posX));
    ui->lbWidth->setText(QString::number(width));

    ui->lbPosX->update();
    ui->lbWidth->update();
}


void ColorDetector::on_btnExit_clicked()
{
    exit(0);
}

void ColorDetector::on_btnCapture_clicked()
{

    captureDevice->take_snapshot();

}


void ColorDetector::setPosColor()
{
    int posx = ui->lbCaptureImage->PointPos.x();
    int posy = ui->lbCaptureImage->PointPos.y();

    QImage getColorImage = captureDevice->imageSnapShot;

    QRgb pixel = getColorImage.pixel(posx,posy);
    QColor color(pixel);

    color = color.toHsv();

    captureDevice->getH = color.hue();
    captureDevice->getS = color.saturation();
    captureDevice->getV = color.value();


    ui->lbPosXY->setText(QString::number(captureDevice->getH) + "," +
                         QString::number(captureDevice->getS) + "," +
                         QString::number(captureDevice->getV));

    QString co = "background-color: hsv(" + QString::number(captureDevice->getH) + "," +
            QString::number(captureDevice->getS) + "," +
            QString::number(captureDevice->getV) +")";

    ui->lbBox4->setStyleSheet(co);
}


void ColorDetector::on_btnGetDetectItem_clicked()
{
    if(!robotics->isRunning())
    {
        ui->btnGetDetectItem->setText("Stop");
        robotics->start();
        ui->btnReady->setEnabled(false);
    } else
    {
        robotics->terminate();
        ui->btnGetDetectItem->setText("Get Detect Object");
        ui->btnReady->setEnabled(true);
    }
}


void ColorDetector::on_btnHelp_clicked()
{
    ui->tabWidget->setTabEnabled(0, false);
    ui->tabWidget->setTabEnabled(1, true);
}

void ColorDetector::on_btnCloseHelp_clicked()
{
    ui->tabWidget->setTabEnabled(0, true);
    ui->tabWidget->setTabEnabled(1, false);
}

void ColorDetector::on_btnReady_clicked()
{
    robotics->set_default(650, 512, 140, 512, 205);
}
