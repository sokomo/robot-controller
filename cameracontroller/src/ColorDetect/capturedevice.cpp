#include <QPainter>
#include <string.h>
#include "capturedevice.h"

CaptureDevice::CaptureDevice(char* deviceName,
                             unsigned char deviceWrapMode,
                             enum capture_method captureMethod)
{
    init_capture_device(deviceName, deviceWrapMode, captureMethod);
    b_snapshot = 0;
    b_start = 0;
    b_startCapture = 0;
    imageFrame = new QImage(QSize(320,240), QImage::Format_RGB888);

    imageSnapShot.scaled(QSize(320,240));
}

CaptureDevice::~CaptureDevice()
{
    end_capture();
    uninit_capture_device();
    delete imageFrame;
}


void CaptureDevice::begin_capture()
{
    init_format_capture(320, 240);
    b_start = 1;
    b_startCapture = 1;

}


void CaptureDevice::end_capture()
{
    b_start = 0;
    b_startCapture = 0;
    stop_capture();
}


void CaptureDevice::take_frame()
{
    //unsigned char *bufData;
    __u32 lengthData = 0;
    //while (1 == b_start)
    {
        capture_frame(imageFrame->bits(), &lengthData);
        //imageFrame->loadFromData(bufData, lengthData);

        if (1 == b_snapshot)
        {
            imageSnapShot = *imageFrame;
            imageSnapShot.scaled(QSize(320,240));
        }
    }
}


void CaptureDevice::take_snapshot()
{
    b_snapshot = 1;
}

unsigned char CaptureDevice::get_snapshot()
{
    return b_snapshot;
}

unsigned char CaptureDevice::get_startCapture()
{
    return b_startCapture;
}



void CaptureDevice::detectColor()
{
    //int flagLR = 0;
    //int flagValid = 0;

    int maxX = -1;
    int minX = 99999;
    int maxY = -1;
    int minY= 99999;
    bool detected = false;

    int width = 0;
    int height = 0;

    chooseColor();
    take_frame();
    if (get_snapshot())
    {
        //QImage capImage = *(captureDevice->imageSnapShot);
        //capImage.load("image.ppm");
        //capImage = capImage.scaled(QSize(320,240));

        emit updateUISnapshot(&imageSnapShot);
        b_snapshot = 0;
    }

    width = imageFrame->width();
    height = imageFrame->height();

    for (int y = 0; y<height; ++y)
    {
        for (int x = 0; x<width; ++x)
        {
            //convert individual pixel to HSV from RGB
            QRgb pixel = imageFrame->pixel(x,y);
            QColor color(pixel);
            color = color.toHsv();

            //default white color for other projects
            //QRgb newPixel = qRgb(255,255,255);

            //Detect color
            if(color.hue() <= HMinColor + 5 && color.hue() >= HMinColor - 5
                    && color.saturation() <= SMinColor + 5 && color.saturation() >= SMinColor -5
                    && color.value() <= VMinColor + 10 && color.value() >= VMinColor -10 )
            {
                detected = true;

                if(x > maxX)
                {
                    maxX = x;
                }else if (x < minX)
                {
                    minX = x;
                }

                if(y > maxY)
                {
                    maxY = y;
                }else if (y < minY)
                {
                    minY = y;
                }

            }
        }

    }

    imageDraw = *imageFrame;
    QRect rect;
    if(detected == true)
    {
        rect = QRect(minX, minY, maxX-minX, maxY-minY);

        //drawing red retangle around detect object
        QPainter painter(&imageDraw);
        painter.setPen(QPen(Qt::red));
        painter.drawRect(rect);
        painter.end();
    }

    emit updateUICapture(&imageDraw);

    if(!rect.isValid())
    {
        mLastRect = QRect();
        mXDist = 0;
        //flagValid = 0;

        strcpy(strGesture, "Not found!");
    }

    int z = rect.x();
    int objWidth = rect.width();
    int objPos = z + (objWidth/2);

    if(z > 0)
    {
        emit updateUIXWidth(objPos, objWidth);

        if(objPos > 230)
        {
            //flagLR = 0;
            strcpy(strGesture, "Right");
        } else if(objPos > 130 && objPos < 230)
        {
            //flagLR = 1;
            strcpy(strGesture, "Center");
        } else if (objPos < 130)
        {
            //flagLR = 2;
            strcpy(strGesture, "Left");
        }
    }else {
        mLastRect = rect;
    }
    QString txtGesture(strGesture);
    emit updateUIGesture(txtGesture);

}


void CaptureDevice::chooseColor(void)
{
    if(flagBox == 1)
    {
        HMinColor = 250;
        SMinColor = 97;
        VMinColor = 37;
    } else if (flagBox == 2)
    {
        HMinColor = 9;
        SMinColor = 181;
        VMinColor = 245;
    }else if (flagBox == 3)
    {
        HMinColor = 74;
        SMinColor = 48;
        VMinColor = 60;
    }else if (flagBox == 4)
    {
        HMinColor = getH;
        SMinColor = getS;
        VMinColor = getV;
    }else
    {
        HMinColor = 0;
        SMinColor = 0;
        VMinColor = 0;
    }

}

void CaptureDevice::run()
{
    while (1)
    {
        take_frame();
        detectColor();
        msleep(36);
    }
}
