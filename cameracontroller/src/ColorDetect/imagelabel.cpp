
#include <QMouseEvent>
#include "imagelabel.h"


ImageLabel::ImageLabel(QWidget * parent, Qt::WindowFlags f) : QLabel(parent, f)
{
}

/*
ImageLabel::ImageLabel(QWidget * parent)
{
    : QLabel(parent);
}
*/

void ImageLabel::mousePressEvent(QMouseEvent *event)
{

    PointPos = event->pos();

    emit mousePressed();

}
