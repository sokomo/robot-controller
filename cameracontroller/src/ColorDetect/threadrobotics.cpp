#include "threadrobotics.h"
#include <rb_backend.h>
#include <iostream>
#include <QFile>
#include <QTextStream>
#include <QStandardItem>
#include <QMessageBox>


#define PIC_OUTOFRANGE 0
#define NECK_VERTICAL 1
#define NECK_HORIZONTAL 2


ThreadRobotics::ThreadRobotics(QObject* parent, char* deviceName, int baudNum)
    : QThread(parent)
{
    open_robotics_device(deviceName, baudNum);
}


ThreadRobotics::~ThreadRobotics()
{
    if (isRunning())
        terminate();
    close_robotics_device();
}


int ThreadRobotics::open_robotics_device(char *deviceName, int baudNum)
{
    open_serial_device(deviceName, baudNum);
    setInitDevice();
    return 1;
}

void ThreadRobotics::close_robotics_device()
{
    close_serial_device();
}

void ThreadRobotics::setInitDevice()
{
    set_speed(MOTOR_GLOBAL_ID, 100);
    sleep(1);    
    move_neck_high_level(512);
}

void ThreadRobotics::loadAutomation()
{

    QString dataText;
    QString pathPrefix("Data/");
    QString fileName("Space");


    int iRow, iCol;
    int index = 0;
    int i = 0;
    int j = 0;
    int sizeData = 0;

    int posData[5];

    QStandardItemModel *model;

    int currWidth = 0;
    int posNeck = 0;




    posMovePlinth[0] = 650;
    posMovePlinth[1] = 550;
    posMovePlinth[2] = 450;
    //posMovePlinth[3] = 350;

    sizeData = sizeof(posMovePlinth) / sizeof(int);

    step_moving_automation(650, 512, 140, 512, 205);

    while (1)
    {

        for (index = 0; index < sizeData; index++)
        {
            rotate_motor(1, posMovePlinth[index]);
            sleep(4);
            //if (textPos == "Center")
            if (QString::compare(lbGesture->text(), QString("Center")) == 0)
            {
                currWidth = lbwidth->text().toInt();

                //std::cout << "Get here.\n";
                //std::cout << "Current Width: " << currWidth << "\n";
                if (currWidth >= 70 && currWidth <= 110)
                    posNeck = NECK_VERTICAL;
                else if (currWidth >= 200 && currWidth <= 250)
                    posNeck = NECK_HORIZONTAL;
                else
                    posNeck = 0;

                if (posNeck > 0)
                {
                    if (posNeck == NECK_VERTICAL)
                        fileName = "SpaceVertical";
                    if (posNeck == NECK_HORIZONTAL)
                        fileName = "SpaceHorizontal";

                    QFile inputFile(pathPrefix + fileName + QString::number(index+1));
                    if(inputFile.open(QIODevice::ReadOnly))
                    {

                        model = new QStandardItemModel(0,0,this);

                        QTextStream in(&inputFile);
                        int rowIndex = 0;
                        while (!in.atEnd())
                        {
                            QString line = in.readLine();
                            QList<QStandardItem *> items;
                            QStringList fields = line.split(" ");
                            foreach (QString text, fields)
                                items.append(new QStandardItem((QString)text));
                            model->appendRow(items);

                            rowIndex++;
                        }
                        inputFile.close();
                        emit updateUITable(model);

                    } else
                    {
                        QMessageBox::information(0,"error",inputFile.errorString());
                    }

                    iRow = model->rowCount();
                    iCol = model->columnCount();

                    for (i = 1; i<iRow; i++)
                    {
                        for (j = 0; j<iCol; j++)
                        {
                            dataText = model->index(i, j, QModelIndex()).data().toString();
                            posData[j] = dataText.toInt();

                        }

                        step_moving_automation(posData[0], posData[1], posData[2],posData[3], posData[4]);
                    }
                    posNeck = 0;
                }
            }
        }
    }

}


void ThreadRobotics::run()
{
    loadAutomation();
}


void ThreadRobotics::set_default(int posPlinth, int posJoint1, int posJoint2,
                                 int posNeck, int posTongs)
{
    step_moving_automation(posPlinth, posJoint1, posJoint2, posNeck, posTongs);
}
