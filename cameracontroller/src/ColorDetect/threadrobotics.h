#ifndef THREADROBOTICS_H
#define THREADROBOTICS_H

#include <QThread>
#include <QString>
#include <QLabel>
#include <QAbstractItemModel>
#include <QStandardItemModel>
#include <QTableView>


class ThreadRobotics : public QThread
{
    Q_OBJECT

signals:
    void updateUITable(QStandardItemModel *newModel);

public:
    ThreadRobotics(QObject* parent = 0, char* deviceName = 0, int baudNum = 57600);
    ~ThreadRobotics();

public:
    QTableView *tableUI;
    QLabel *lbwidth;
    QLabel *lbGesture;
    int posMovePlinth[3];

public:
    int open_robotics_device(char* deviceName, int baudNum);
    void close_robotics_device();
    void set_default(int posPlinth, int posJoint1, int posJoint2,
                     int posNeck, int posTongs);



private:
    void setInitDevice();
    void loadAutomation();

protected:
    void run();

};

#endif // THREADROBOTICS_H
