#ifndef COLORDETECTOR_H
#define COLORDETECTOR_H
#include <QImage>

#include <QMainWindow>
#include "threadrobotics.h"
#include "capturedevice.h"
#include <QStandardItemModel>

namespace Ui {
class ColorDetector;
}

class ColorDetector : public QMainWindow
{
    Q_OBJECT


public:
    explicit ColorDetector(QWidget *parent = 0);
    ~ColorDetector();

private slots:
    void on_rdbBox1_clicked();

    void on_rdbBox2_clicked();

    void on_btnStart_clicked();

    void on_rdbBox3_clicked();

    void on_btnExit_clicked();

    void on_btnCapture_clicked();

    void on_rdbBox4_clicked();

    void setPosColor();

    void on_btnGetDetectItem_clicked();

    void on_btnHelp_clicked();

    void on_btnCloseHelp_clicked();

    void on_btnReady_clicked();

    void updateUIRobotics(QStandardItemModel *newModel);

    void updateUICapture(QImage *frameCapture);

    void updateUIGesture(QString strGesture);

    void updateUIXWidth(int posX, int width);

    void updateUISnapshot(QImage *imgSnapshot);

private:
    Ui::ColorDetector *ui;

private:
    void initial_values(void);
    void detectColor(void);
    void detectGesture(void);
    void chooseColor(void);
    void delay(void);

public:
    QStandardItemModel *model;

private:
    QRect mLastRect;
    int mXDist;

private:
    int flagValid;
    int flagLR;
    int flagBox;
    int flagFunction;

    int HMaxColor;
    int HMinColor;
    int SMaxColor;
    int SMinColor;
    int VMaxColor;
    int VMinColor;

    int getH;
    int getS;
    int getV;
    int HCustom;
    int SCustom;
    int VCustom;
    int flagStart;
private:
    ThreadRobotics* robotics;
    CaptureDevice* captureDevice;
    //int posData[4];

};

#endif // COLORDETECTOR_H
