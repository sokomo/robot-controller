#ifndef CAPTUREDEVICE_H
#define CAPTUREDEVICE_H

#include <QThread>
#include <QImage>
#include <camera_low_level.h>

class CaptureDevice : public QThread
{
    Q_OBJECT

signals:
    void updateUICapture(QImage *frameCapture);
    void updateUIGesture(QString strGesture);
    void updateUIXWidth(int posX, int width);
    void updateUISnapshot(QImage *imgSnapshot);

public:
    CaptureDevice(char* deviceName = 0,
                  unsigned char deviceWrapMode = 1,
                  enum capture_method captureMethod = methodMmap);

    ~CaptureDevice();

protected:
    void run();

public:
    void init_device();
    void begin_capture();
    void end_capture();
    void take_frame();
    void take_snapshot();
    unsigned char get_snapshot();
    unsigned char get_startCapture();

public:
    QImage imageSnapShot;
    unsigned char b_snapshot;


private:
    QImage* imageFrame;
    QImage imageDraw;
    char strGesture[13];
    unsigned char b_start;
    unsigned char b_startCapture;

private:
    QRect mLastRect;
    int mXDist;
    //int flagValid;
    //int flagLR;
    //int flagFunction;

    //int HMaxColor;
    int HMinColor;
    //int SMaxColor;
    int SMinColor;
    //int VMaxColor;
    int VMinColor;

    //int HCustom;
    //int SCustom;
    //int VCustom;

public:
    int getH;
    int getS;
    int getV;
    int flagBox;

private:
    void chooseColor();
    void detectColor();

};


#endif // CAPTUREDEVICE_H
