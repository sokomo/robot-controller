#ifndef IMAGELABEL_H
#define IMAGELABEL_H

#include <QLabel>
#include <QPoint>

class ImageLabel : public QLabel
{
    Q_OBJECT

signals:
    void mousePressed();

public:
    //int PosX;
    //int PosY;
    QPoint PointPos;

public:
    ImageLabel(QWidget * parent = 0, Qt::WindowFlags f = 0);
    //ImageLabel(QWidget * parent = 0);
    //~ImageLabel() : ~QLabel() {};

protected:
    void mousePressEvent(QMouseEvent *event);
};


#endif // IMAGELABEL_H
