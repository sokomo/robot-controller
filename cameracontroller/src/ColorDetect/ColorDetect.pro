#-------------------------------------------------
#
# Project created by QtCreator 2013-11-11T09:49:24
# Make by TungNV60382
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ColorDetect
TEMPLATE = app


SOURCES += main.cpp\
        colordetector.cpp \
    imagelabel.cpp \
    threadrobotics.cpp \
    capturedevice.cpp

HEADERS  += colordetector.h \
    imagelabel.h \
    threadrobotics.h \
    capturedevice.h

FORMS    += colordetector.ui

QMAKE_CXXFLAGS +=

INCLUDEPATH += ../../include

LIBS += -L../lib -lrbbackend -ldxl -lcapture -lv4lconvert -lv4l2 -lm

RESOURCES +=
