/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h> /* Define declaration read write file  */

#include "color_terminal_definition.h"

#include <capture_api.h>


/* Variable Definition */
extern int errno;      /* Use global error variable */
int g_fdCaptureDevice = -1;
/* End definition */

unsigned char g_deviceWrapMode = 0;
enum capture_method g_captureMethod = methodMmap;


void print_errno_message()
{
  fprintf(stderr, ANSI_COLOR_RED "Error ID: %d -  %s\n" ANSI_COLOR_RESET,
          errno, strerror(errno));
}

void print_error_normal_message(char *err_message)
{
  fprintf(stderr, ANSI_COLOR_RED "%s\n" ANSI_COLOR_RESET, err_message);
}


int getDeviceCaptureFile(char *deviceName)
{
  int filefd = -1;
  unsigned int limitData = 100;
  char fileName[] = "deviceCapture.conf";
  char strData[limitData];

  if ((filefd = open(fileName, O_RDONLY)) < 0)
  {
    print_errno_message();
    return -1;
  }

  if (read(filefd, strData, limitData) < 0)
  {
    print_errno_message();
    close(filefd);
    return -1;
  }

  sscanf(strData, "%s", deviceName);
  close(filefd);

  return 1;
}


enum capture_method get_capture_method()
{
  return g_captureMethod;
}


int open_capture_device(char *device, unsigned char deviceOpenMode, enum capture_method captureMethod)
{
  g_deviceWrapMode = deviceOpenMode;
  g_captureMethod = captureMethod;
  int resultFd = 0;
  char deviceNameFile[100];

  if (getDeviceCaptureFile(deviceNameFile) > 0)
    g_fdCaptureDevice = open(deviceNameFile, O_RDWR | O_NONBLOCK);

  if (g_fdCaptureDevice < 0)
    g_fdCaptureDevice = open(device, O_RDWR | O_NONBLOCK);

  if (g_fdCaptureDevice < 0)
  {
    print_errno_message();
    return 0;
  }

  if (g_deviceWrapMode == 1)
  {
    resultFd = v4l2_fd_open(g_fdCaptureDevice, V4L2_ENABLE_ENUM_FMT_EMULATION);
    if (resultFd < 0)
    {
      g_deviceWrapMode = 0;
      print_error_normal_message("Cannot use libv4l2 wrapper.\n");
    }
  }

  fprintf(stdout, "Open capture device %s successfully.\n", device);
  return 1;
}


void close_capture_device()
{
  if (g_deviceWrapMode == 1)
    v4l2_close(g_fdCaptureDevice);
  else
    close(g_fdCaptureDevice);

  g_fdCaptureDevice = -1;
}

int ioctl_capture(unsigned long int cmd, void *arg)
{
  if (g_deviceWrapMode == 1)
    return v4l2_ioctl(g_fdCaptureDevice, cmd, arg);
  return ioctl(g_fdCaptureDevice, cmd, arg);
}

int ioctl_des_capture(char *descr, unsigned long int cmd, void *arg)
{
  int err = 0;
  err = ioctl_capture(cmd, arg);

  if (err < 0)
  {
    print_error_normal_message(descr);
    print_errno_message();
  }
  return err >= 0;
}

int ioctl_exists_capture(unsigned long int cmd, void *arg)
{
  int err = 0;

  if (g_deviceWrapMode == 1)
    err = v4l2_ioctl(g_fdCaptureDevice, cmd, arg);
  else
    err = ioctl(g_fdCaptureDevice, cmd, arg);
  return ( !err || errno != ENOTTY );
}


int read_capture(void *capBuffer, int dataSize)
{
  if (g_deviceWrapMode == 1)
    return v4l2_read(g_fdCaptureDevice, capBuffer, dataSize);
  return read(g_fdCaptureDevice, capBuffer, dataSize);
}


void *mmap_capture(size_t lengthData, int64_t offsetData)
{
  if (g_deviceWrapMode == 1)
    return v4l2_mmap(NULL, lengthData, PROT_READ | PROT_WRITE, MAP_SHARED,
                     g_fdCaptureDevice, offsetData);
  return mmap(NULL, lengthData, PROT_READ | PROT_WRITE, MAP_SHARED,
              g_fdCaptureDevice, offsetData);
}


int munmap_capture(void *startBuffer, size_t lengthData)
{
  if (g_fdCaptureDevice == 1)
    return v4l2_munmap(startBuffer, lengthData);
  return munmap(startBuffer, lengthData);
}


int reqbufs_user(struct v4l2_requestbuffers *reqbuf, __u32 bufType, int countBuf)
{
  memset(reqbuf, 0, sizeof(*reqbuf));
  reqbuf->type = bufType;
  reqbuf->memory = V4L2_MEMORY_USERPTR;
  reqbuf->count = countBuf;

  return ioctl_capture(VIDIOC_REQBUFS, reqbuf) >= 0;
}


int reqbufs_mmap(struct v4l2_requestbuffers *reqbuf, __u32 bufType, int countBuf)
{
  memset(reqbuf, 0, sizeof(*reqbuf));
  reqbuf->type = bufType;
  reqbuf->memory = V4L2_MEMORY_MMAP;
  reqbuf->count = countBuf;

  return ioctl_capture(VIDIOC_REQBUFS, reqbuf) >= 0;
}

int deqbuf_user(struct v4l2_buffer *bufCapture, __u32 bufType, unsigned char *flagAgain)
{
  int res;

  memset(bufCapture, 0, sizeof(*bufCapture));
  bufCapture->type = bufType;
  bufCapture->memory = V4L2_MEMORY_USERPTR;

  res = ioctl_capture(VIDIOC_DQBUF, bufCapture);
  *flagAgain = res < 0 && errno == EAGAIN;
  return res >= 0 || *flagAgain;
}


int deqbuf_mmap(struct v4l2_buffer *bufCapture, __u32 bufType, unsigned char *flagAgain)
{
  int res;

  memset(bufCapture, 0, sizeof(*bufCapture));
  bufCapture->type = bufType;
  bufCapture->memory = V4L2_MEMORY_MMAP;

  res = ioctl_capture(VIDIOC_DQBUF, bufCapture);
  *flagAgain = res < 0 && errno == EAGAIN;
  return res >= 0 || *flagAgain;
}


int qbuf_user(int indexBuf, __u32 bufType, void *bufData, int lengthData)
{
  struct v4l2_buffer bufTemp;

  memset(&bufTemp, 0, sizeof(bufTemp));
  bufTemp.type = bufType;
  bufTemp.memory = V4L2_MEMORY_USERPTR;
  bufTemp.m.userptr = (unsigned long) bufData;
  bufTemp.length = lengthData;
  bufTemp.index = indexBuf;

  return ioctl_capture(VIDIOC_QBUF, &bufTemp) >= 0;
}


int qbuf_mmap(int indexBuf, __u32 bufType)
{
  struct v4l2_buffer bufTemp;

  memset(&bufTemp, 0, sizeof(bufTemp));
  bufTemp.type = bufType;
  bufTemp.memory = V4L2_MEMORY_MMAP;
  bufTemp.index = indexBuf;

  return ioctl_capture(VIDIOC_QBUF, &bufTemp) >= 0;
}


int streamon_capture(__u32 bufType)
{
  return ioctl_des_capture("Problem in stream on.\n",VIDIOC_STREAMON, &bufType);
}


int streamoff_capture(__u32 bufType)
{
  return ioctl_des_capture("Problem in stream off.\n", VIDIOC_STREAMOFF, &bufType);
}


int get_format_capture(struct v4l2_format *fmt)
{
  memset(fmt, 0, sizeof(*fmt));
  fmt->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  return ioctl_capture(VIDIOC_G_FMT, fmt) >= 0;
}

int set_format_capture(struct v4l2_format *fmt)
{
  fmt->fmt.pix.bytesperline = 0;
  return ioctl_capture(VIDIOC_S_FMT, fmt);
}

int get_capture_interval(struct v4l2_fract *capInterval)
{
  struct v4l2_streamparm strParm;

  strParm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl_capture(VIDIOC_G_PARM, &strParm) >= 0 &&
      (strParm.parm.capture.capability & V4L2_CAP_TIMEPERFRAME))
  {
    *capInterval = strParm.parm.capture.timeperframe;
    return 1;
  }

  return 0;
}

int set_capture_interval(struct v4l2_fract *capInterval)
{
  struct v4l2_streamparm strParm;

  strParm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl_capture(VIDIOC_G_PARM, &strParm) < 0)
    return 0;

  if (!(strParm.parm.capture.capability & V4L2_CAP_TIMEPERFRAME))
    return 0;

  strParm.parm.capture.timeperframe = *capInterval;
  return ioctl_capture(VIDIOC_S_PARM, &strParm) >= 0;
}
