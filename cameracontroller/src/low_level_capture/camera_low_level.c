/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <camera_low_level.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <linux/types.h>
#include <libv4lconvert.h>

#define BUFFER_SIZE_CAPTURE 3

#define LOW_LEVEL_API_CAPTURE 1


__u32 g_bufType = V4L2_BUF_TYPE_VIDEO_CAPTURE;
struct buffer *g_buffersCapture = 0;
unsigned int g_nBuffers = 0;
struct v4lconvert_data *g_convertData;

struct v4l2_format g_capSrcFormat;
struct v4l2_format g_capDestFormat;

/* Extern global variable */
extern int g_fdCaptureDevice;
extern enum capture_method g_captureMethod;



int init_capture_device(char *deviceName, unsigned char deviceWrapMode, enum capture_method captureMethod)
{
  if (open_capture_device(deviceName, deviceWrapMode, captureMethod) < 0)
    return 0;

  g_convertData = v4lconvert_create(g_fdCaptureDevice);
  return 1;
}

void init_format_capture(__u32 width, __u32 height)
{
  
  struct v4l2_format backupFormat;
  struct v4l2_fract captureInverval;

  get_format_capture(&g_capSrcFormat);
  set_format_capture(&g_capSrcFormat);

  g_capSrcFormat.fmt.pix.width = width;
  g_capSrcFormat.fmt.pix.height = height;
  g_capDestFormat = g_capSrcFormat;
  g_capDestFormat.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;

  backupFormat = g_capSrcFormat;
  
  v4lconvert_try_format(g_convertData, &g_capDestFormat, &g_capSrcFormat);
  /* v4lconvert_try_format sometimes modifies the source format if it thinks
	 * that there is a better format available. Restore our selected source
   * format since we do not want that happening.
   */

  g_capSrcFormat = backupFormat;
  set_format_capture(&g_capSrcFormat);

  /* get_capture_interval(&captureInverval);

  fprintf(stdout, "Denominator: %d, Numerator: %d\n",
          captureInverval.denominator, captureInverval.numerator);
  */

  start_capture(g_capSrcFormat.fmt.pix.sizeimage);

}

void uninit_capture_device()
{
  close_capture_device();
  v4lconvert_destroy(g_convertData);
}


int start_capture(unsigned long int bufferSize)
{
  struct v4l2_requestbuffers reqBuf;
  unsigned int indexes = 0;

  //memset(&reqBuf, 0, sizeof(reqBuf));

  switch (g_captureMethod)
  {
  case methodMmap:
    if (!reqbufs_mmap(&reqBuf, g_bufType, BUFFER_SIZE_CAPTURE))
    {
      print_error_normal_message("Cannot request buffers.\n");
      print_errno_message();
      break;
    }

    if (reqBuf.count < (BUFFER_SIZE_CAPTURE))
    {
      print_error_normal_message("Too few buffers.\n");
      reqbufs_mmap(&reqBuf, g_bufType, 0);
      break;
    }

    g_buffersCapture = (struct buffer*) calloc(reqBuf.count, sizeof(*g_buffersCapture));

    if (!g_buffersCapture)
    {
      print_error_normal_message("Out of memory.\n");
      reqbufs_mmap(&reqBuf, g_bufType, 0);
      break;
    }

    for (g_nBuffers = 0; g_nBuffers < reqBuf.count; ++g_nBuffers)
    {
      struct v4l2_buffer bufTemp;

      memset(&bufTemp, 0, sizeof(bufTemp));

      bufTemp.type = g_bufType;
      bufTemp.memory = V4L2_MEMORY_MMAP;
      bufTemp.index = g_nBuffers;


      if (-1 == ioctl_capture(VIDIOC_QUERYBUF, &bufTemp))
      {
        print_error_normal_message("Cannot query buffer.\n");
        return 0;
      }

      g_buffersCapture[g_nBuffers].length = bufTemp.length;
      g_buffersCapture[g_nBuffers].start = mmap_capture(bufTemp.length, bufTemp.m.offset);

      if (MAP_FAILED == g_buffersCapture[g_nBuffers].start)
      {
        print_error_normal_message("Cannot map memory.\n");
        return 0;
      }
    }

    for (indexes = 0; indexes < g_nBuffers; ++indexes)
    {
      if (!qbuf_mmap(indexes, g_bufType))
      {
        print_error_normal_message("Error in queue buffer.\n");
        print_errno_message();
        return 0;
      }
    }

    if (!streamon_capture(g_bufType))
    {
      print_error_normal_message("Cannot stream on.\n");
      return 0;
    }
    return 1;


  case methodUser:
    if (!reqbufs_user(&reqBuf, g_bufType, BUFFER_SIZE_CAPTURE))
    {
      print_error_normal_message("Cannot capture.\n");
      break;
    }

    if (reqBuf.count < (BUFFER_SIZE_CAPTURE - 1))
    {
      print_error_normal_message("Too few buffers.\n");
      reqbufs_user(&reqBuf, g_bufType, 0);
      break;
    }

    g_buffersCapture = (struct buffer*) calloc(reqBuf.count, sizeof(*g_buffersCapture));

    if (!g_buffersCapture)
    {
      print_error_normal_message("Out of memory.\n");
      break;
    }

    for (g_nBuffers = 0; g_nBuffers < reqBuf.count; ++g_nBuffers)
    {
      g_buffersCapture[g_nBuffers].length = bufferSize;
      g_buffersCapture[g_nBuffers].start = malloc(bufferSize);

      if (!g_buffersCapture[g_nBuffers].start)
      {
        print_error_normal_message("Out of memory.\n");
        return 0;
      }
    }

    for (indexes = 0; indexes < g_nBuffers; ++indexes)
    {
      if (!qbuf_user(indexes, g_bufType, g_buffersCapture[indexes].start, g_buffersCapture[indexes].length))
      {
        print_error_normal_message("Cannot queue buffer.\n");
        return 0;
      }
    }

    if (!streamon_capture(g_bufType))
    {
      print_error_normal_message("Cannot stream on.\n");
      return 0;
    }
    return 1;

  }

  return 0;
}


void stop_capture()
{
  struct v4l2_requestbuffers reqBuf;
  //struct v4l2_encoder_cmd cmdEncoder;
  unsigned int indexes;

  switch(g_captureMethod)
  {
  case methodMmap:
    if (g_buffersCapture == NULL)
      break;

    if (!streamoff_capture(g_bufType))
      print_error_normal_message("Cannot stream off.'n");

    for (indexes = 0; indexes < g_nBuffers; ++indexes)
      if (-1 == munmap_capture(g_buffersCapture[indexes].start, g_buffersCapture[indexes].length))
        print_error_normal_message("Cannot unmap memory.\n");

    reqbufs_mmap(&reqBuf, g_bufType, 1);
    reqbufs_mmap(&reqBuf, g_bufType, 0);

    break;

  case methodUser:
    if (!streamoff_capture(g_bufType))
    print_error_normal_message("Cannot stream off.\n");

    reqbufs_user(&reqBuf, g_bufType, 1);
    reqbufs_user(&reqBuf, g_bufType, 0);

    for (indexes = 0; indexes < g_nBuffers; ++indexes)
      free(g_buffersCapture[indexes].start);

    break;

  }
  free(g_buffersCapture);
  g_buffersCapture = NULL;
}


int capture_frame(unsigned char* destData, __u32 *lengthData)
{
  struct v4l2_buffer bufTemp;
  int err = 0;
  unsigned char flagAgain = 0;

  switch(g_captureMethod)
  {
  case methodMmap:
    if (!deqbuf_mmap(&bufTemp, g_bufType, &flagAgain))
    {
      print_error_normal_message("Cannot deque buffer.\n");
      return 0;
    }

    if (flagAgain)
      return 1;

    err = v4lconvert_convert(g_convertData, &g_capSrcFormat, &g_capDestFormat,
                             (unsigned char *) g_buffersCapture[bufTemp.index].start,
                             bufTemp.bytesused, destData, g_capDestFormat.fmt.pix.sizeimage);

    if (err < 0)
      memcpy(destData, g_buffersCapture[bufTemp.index].start, bufTemp.bytesused);

    //g_bufferFrame = (g_buffersCapture + bufTemp.index);

    //destData = (unsigned char *) g_buffersCapture[bufTemp.index].start;
    *lengthData = bufTemp.bytesused;

    qbuf_mmap(bufTemp.index, g_bufType);

    break;

  case methodUser:
    if (!deqbuf_user(&bufTemp, g_bufType, &flagAgain))
    {
      print_error_normal_message("Cannot deque buffer.\n");
      return 0;
    }

    if (flagAgain)
      return 1;


    err = v4lconvert_convert(g_convertData, &g_capSrcFormat, &g_capDestFormat,
                             (unsigned char *) bufTemp.m.userptr,
                             bufTemp.bytesused, destData, g_capDestFormat.fmt.pix.sizeimage);

    if (err < 0)
      memcpy(destData, (unsigned char *)bufTemp.m.userptr, bufTemp.bytesused);

    //g_bufferFrame =

    destData = (unsigned char *) bufTemp.m.userptr;
    *lengthData = bufTemp.bytesused;

    qbuf_mmap(bufTemp.index, g_bufType);

    break;

  }

  return 1;
}

