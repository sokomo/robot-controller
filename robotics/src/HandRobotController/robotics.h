#ifndef ROBOTICS_H
#define ROBOTICS_H

#include <QThread>
#include <QAbstractItemModel>
#include <rb_backend.h>

class Robotics : public QThread
 {
     Q_OBJECT

public:
    Robotics(QObject *parent = 0, char *deviceName = 0, int baudNum = 57600);
    ~Robotics();

protected:
     void run();

private:
     void init_robot_device();
     void loadAutomation();
     void close_device();

public:
     void change_speed(int motorid, int value);
     void movePlinth(int pos);
     void moveJoint1(int pos);
     void moveJoint2(int pos);
     void moveNeck(int pos);
     void moveTongs(int pos);
     void moveAll(int posPlinth, int posJoint1, int posJoint2,
                  int posNeck, int posTongs);

public:
    QAbstractItemModel *tableModel;

 };




#endif // ROBOTICS_H
