#include "robotics.h"
#include <QAbstractItemModel>
#include <rb_backend.h>


Robotics::Robotics( QObject *parent, char *deviceName, int baudNum)
    : QThread(parent)
{
    open_serial_device(deviceName, baudNum);
    sleep(1);
    init_robot_device();
}

Robotics::~Robotics()
{
    if (isRunning())
        terminate();
    close_serial_device();
}

void Robotics::change_speed(int motorid, int value)
{
    set_speed(motorid, value);
}


void Robotics::init_robot_device()
{
    change_speed(MOTOR_GLOBAL_ID, 100);
}


void Robotics::movePlinth(int pos)
{
    move_plinth_high_level(pos);
}

void Robotics::moveJoint1(int pos)
{
    int idList[2] = { MOTOR_JOINT1_1, MOTOR_JOINT1_2 };
    int posList [2] = { pos + 62, pos };
    int sizeArray = sizeof(idList) / sizeof(int);
    //rotate_multi_motors_same_value(idList, sizeArray, pos);
    rotate_multi_motors(idList, sizeArray, posList);
}


void Robotics::moveJoint2(int pos)
{
    int idList[2] = { MOTOR_JOINT2_1, MOTOR_JOINT2_2 };
    int sizeArray = sizeof(idList) / sizeof(int);
    rotate_multi_motors_same_value(idList, sizeArray, pos);

}


void Robotics::moveNeck(int pos)
{
    move_neck_high_level(pos);
}

void Robotics::moveTongs(int pos)
{
    move_tongs_high_level(pos);
}


void Robotics::moveAll(int posPlinth, int posJoint1, int posJoint2,
                       int posNeck, int posTongs)
{
    /*
    int idList[7] = {
        MOTOR_PLINTH, MOTOR_JOINT1_1, MOTOR_JOINT1_2,
        MOTOR_JOINT2_1, MOTOR_JOINT2_2, MOTOR_NECK, MOTOR_TONGS };

    int speedList[7] = {
        posPlinth, posJoint1, posJoint1, posJoint2, posJoint2, posNeck, posTongs };

    int sizeArray = sizeof(idList) / sizeof(int);
    rotate_multi_motors(idList, sizeArray, speedList);
    */
    step_moving_automation(posPlinth, posJoint1, posJoint2, posNeck, posTongs);

}

void Robotics::loadAutomation()
{

    QString dataText;
    int iRow, iCol;

    int posData[5];



    iRow = tableModel->rowCount();
    iCol = tableModel->columnCount();

    while (1)
    {

        for (int i = 0; i<iRow; i++)
        {
            for (int j = 0; j<iCol; j++)
            {
                dataText = tableModel->index(i, j, QModelIndex()).data().toString();
                posData[j] = dataText.toInt();
                //std::cout << dataText.toStdString() << " ";

                //QModelIndex cellValue[i][j] = model->index(i,j,QModelIndex());


            }

            step_moving_automation(posData[0], posData[1], posData[2], posData[3], posData[4]);
        }
    }
}

void Robotics::run()
{
    loadAutomation();
}
