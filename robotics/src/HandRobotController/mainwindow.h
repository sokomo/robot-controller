#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>
#include <QThread>
#include <robotics.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public:
    QStandardItemModel *model;

private slots:

    /* Manual events */
    void on_sldValue1_valueChanged(int value);

    void on_txtValue1_textChanged();

    void on_sldValue2_valueChanged(int value);

    void on_sldValue3_valueChanged(int value);

    void on_sldValue4_valueChanged(int value);

    void on_sldValue5_valueChanged(int value);

    void on_btnOK1_pressed();

    void on_btnOK2_pressed();

    void on_btnOK3_pressed();

    void on_btnOK4_pressed();

    void on_btnCancel1_pressed();

    void on_btnCancel2_pressed();

    void on_btnCancel3_pressed();

    void on_btnCancel4_pressed();

    void on_btnDoAll_pressed();


    /* Automation events */
    void on_sldValueAuto1_valueChanged(int value);

    void on_sldValueAuto2_valueChanged(int value);

    void on_sldValueAuto3_valueChanged(int value);

    void on_sldValueAuto4_valueChanged(int value);

    void on_sldValueAuto5_valueChanged(int value);

    void on_btnAutoStart_pressed();

    void on_btnOK5_pressed();

    void on_btnCancel5_pressed();

    void on_btnOKAuto1_pressed();

    void on_btnOKAuto2_pressed();

    void on_btnOKAuto3_pressed();

    void on_btnOKAuto4_pressed();

    void on_btnOKAuto5_pressed();

    void on_btnSave_pressed();

    void on_btnLoad_pressed();

    void on_btnExit_pressed();


    /* Gamepad events */
    void on_btnGPUp1_pressed();

    void on_btnGPUp1_released();

    void on_btnGPLeft_pressed();

    void on_btnGPLeft_released();

    void on_btnGPDown1_pressed();

    void on_btnGPDown1_released();

    void on_btnGPRight_pressed();

    void on_btnGPRight_released();

    void on_btnGPUp2_pressed();

    void on_btnGPUp2_released();

    void on_btnGPDown2_pressed();

    void on_btnGPDown2_released();

    void on_btnGPGrab_pressed();

    void on_btnGPGrab_released();

    void on_btnGPDrop_pressed();

    void on_btnGPDrop_released();

    void on_btnGPRollLeft_pressed();

    void on_btnGPRollLeft_released();

    void on_btnGPRollRight_pressed();

    void on_btnGPRollRight_released();

    void on_sldGPValue1_valueChanged(int value);

    void on_sldGPValue2_valueChanged(int value);

    void on_sldGPValue3_valueChanged(int value);

    void on_sldGPValue4_valueChanged(int value);

    void on_sldGPValue5_valueChanged(int value);

    void on_btnGPStart_clicked();

    void on_btnHelp_clicked();

    void on_btnCloseHelp_clicked();

    void on_btnValue11_pressed();

    void on_btnValue11_released();

    void on_btnValue10_pressed();

    void on_btnValue10_released();

    void on_btnValue21_pressed();

    void on_btnValue21_released();

    void on_btnValue20_pressed();

    void on_btnValue20_released();

    void on_btnValue31_pressed();

    void on_btnValue31_released();

    void on_btnValue30_pressed();

    void on_btnValue30_released();

    void on_btnValue41_pressed();

    void on_btnValue41_released();

    void on_btnValue40_pressed();

    void on_btnValue40_released();

    void on_btnValue51_pressed();

    void on_btnValue51_released();

    void on_btnValue50_pressed();

    void on_btnValue50_released();

    void on_btnSp11_pressed();

    void on_btnSp11_released();

    void on_btnSp10_pressed();

    void on_btnSp10_released();

    void on_btnSp11_2_pressed();

    void on_btnSp11_2_released();

    void on_btnSp10_2_pressed();

    void on_btnSp10_2_released();

    void on_btnSp11_3_pressed();

    void on_btnSp11_3_released();

    void on_btnSp10_3_pressed();

    void on_btnSp10_3_released();

    void on_btnSp11_4_pressed();

    void on_btnSp11_4_released();

    void on_btnSp10_4_pressed();

    void on_btnSp10_4_released();

    void on_btnSp11_5_pressed();

    void on_btnSp11_5_released();

    void on_btnSp10_5_pressed();

    void on_btnSp10_5_released();

    void on_spSpeed1_valueChanged(int value);

    void on_spSpeed2_valueChanged(int value);

    void on_spSpeed3_valueChanged(int value);

    void on_spSpeed4_valueChanged(int value);

    void on_spSpeed5_valueChanged(int value);

    void on_tabWidget_currentChanged(int index);

private:
    Ui::MainWindow *ui;

    int flagAdd;
    int flagGPValue1;
    int flagGPValue2;
    int flagGPValue3;
    int flagGPValue4;
    int flagGPValue5;
    int flagGPStart;

    //int idList[7];
    int iRow;
    int iCol;

    int cellValue;
    int threadAutomationFlag;

    int gpValue1;
    int gpValue2;
    int gpValue3;
    int gpValue4;
    int gpValue5;


private:
    Robotics* roboticsthread;

private:
    void delay();
    void initial_values();


};

#endif // MAINWINDOW_H
