#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include <rb_backend.h>
//#include <robotics.h>

#include <time.h>
#include <unistd.h>
#include <iostream>
#include <QFile>
#include <QTextStream>
#include <QTableWidget>
#include <QMessageBox>
#include <QString>
#include <QDebug>
#include <QStandardItemModel>
#include <QTime>
#include <QFileDialog>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    initial_values();

    char deviceName[100] = "/dev/ttyUSB0";

    roboticsthread = new Robotics(0, deviceName, 57600);

}


void MainWindow::initial_values()
{
    ui->txtValue1->setText("Group 6");
    ui->txtValue1->setAlignment(Qt::AlignCenter);

    QPixmap *pm= new QPixmap(":/new/prefix1/Image/RobotArm1.png");
    QPixmap p1(pm->scaled ( 141,141, Qt::IgnoreAspectRatio, Qt::SmoothTransformation ));
    ui->lbImage1->setPixmap(p1);
    ui->lbImage1->show();
    ui->lbImage1->setFixedHeight(p1.height());
    ui->lbImage1->setFixedWidth(p1.width());

    QPixmap *pm2=new QPixmap(":/new/prefix1/Image/RobotArm2.png");
    QPixmap p2(pm2->scaled ( 141,141, Qt::IgnoreAspectRatio, Qt::SmoothTransformation ));
    ui->lbImage2->setPixmap(p2);
    ui->lbImage2->show();
    ui->lbImage2->setFixedHeight(p2.height());
    ui->lbImage2->setFixedWidth(p2.width());

    QPixmap *pm3=new QPixmap(":/new/prefix1/Image/RobotArm3.png");
    QPixmap p3(pm3->scaled ( 141,141, Qt::IgnoreAspectRatio, Qt::SmoothTransformation ));
    ui->lbImage3->setPixmap(p3);
    ui->lbImage3->show();
    ui->lbImage3->setFixedHeight(p3.height());
    ui->lbImage3->setFixedWidth(p3.width());

    QPixmap *pm4=new QPixmap(":/new/prefix1/Image/RobotArm4.png");
    QPixmap p4(pm4->scaled ( 141,141, Qt::IgnoreAspectRatio, Qt::SmoothTransformation ));
    ui->lbImage4->setPixmap(p4);
    ui->lbImage4->show();
    ui->lbImage4->setFixedHeight(p4.height());
    ui->lbImage4->setFixedWidth(p4.width());

    QPixmap *pm5=new QPixmap(":/new/prefix1/Image/RobotArm5.png");
    QPixmap p5(pm5->scaled ( 141,141, Qt::IgnoreAspectRatio, Qt::SmoothTransformation ));
    ui->lbImage5->setPixmap(p5);
    ui->lbImage5->show();
    ui->lbImage5->setFixedHeight(p5.height());
    ui->lbImage5->setFixedWidth(p5.width());

    ui->spValue1->setMinimum(ui->sldValue1->minimum());
    ui->spValue2->setMinimum(ui->sldValue2->minimum());
    ui->spValue3->setMinimum(ui->sldValue3->minimum());
    ui->spValue4->setMinimum(ui->sldValue4->minimum());
    ui->spValue5->setMinimum(ui->sldValue5->minimum());

    ui->spValue1->setMaximum(ui->sldValue1->maximum());
    ui->spValue2->setMaximum(ui->sldValue2->maximum());
    ui->spValue3->setMaximum(ui->sldValue3->maximum());
    ui->spValue4->setMaximum(ui->sldValue4->maximum());
    ui->spValue5->setMaximum(ui->sldValue5->maximum());

    ui->spValueAuto1->setMinimum(ui->sldValueAuto1->minimum());
    ui->spValueAuto2->setMinimum(ui->sldValueAuto2->minimum());
    ui->spValueAuto3->setMinimum(ui->sldValueAuto3->minimum());
    ui->spValueAuto4->setMinimum(ui->sldValueAuto4->minimum());
    ui->spValueAuto5->setMinimum(ui->sldValueAuto5->minimum());

    ui->spValueAuto1->setMaximum(ui->sldValueAuto1->maximum());
    ui->spValueAuto2->setMaximum(ui->sldValueAuto2->maximum());
    ui->spValueAuto3->setMaximum(ui->sldValueAuto3->maximum());
    ui->spValueAuto4->setMaximum(ui->sldValueAuto4->maximum());
    ui->spValueAuto5->setMaximum(ui->sldValueAuto5->maximum());

    ui->lbValue1->setText("30 <=> 1000");
    ui->lbValue1->setAlignment(Qt::AlignCenter);
    ui->lbValue2->setText("90 <=> 900");
    ui->lbValue2->setAlignment(Qt::AlignCenter);
    ui->lbValue3->setText("60 <=> 900");
    ui->lbValue3->setAlignment(Qt::AlignCenter);
    ui->lbValue4->setText("200 <=> 800");
    ui->lbValue4->setAlignment(Qt::AlignCenter);
    ui->lbValue5->setText("205 <=> 375");
    ui->lbValue5->setAlignment(Qt::AlignCenter);


    model = new QStandardItemModel(0,0,this);
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Value 1")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Value 2")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Value 3")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Value 4")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Value 5")));

    ui->tableView->setModel(model);

    flagGPValue1 = 0;
    flagGPValue2 = 0;
    flagGPValue3 = 0;
    flagGPValue4 = 0;
    flagGPValue5 = 0;

    flagGPStart = 0;

    flagAdd = 0;

    gpValue1 = ui->sldGPValue1->value();
    gpValue2 = ui->sldGPValue2->value();
    gpValue3 = ui->sldGPValue3->value();
    gpValue4 = ui->sldGPValue4->value();
    gpValue5 = ui->sldGPValue5->value();

    ui->lableGP1->setText(QString::number(ui->sldGPValue1->value()));
    ui->lableGP2->setText(QString::number(ui->sldGPValue2->value()));
    ui->lableGP3->setText(QString::number(ui->sldGPValue3->value()));
    ui->lableGP4->setText(QString::number(ui->sldGPValue4->value()));
    ui->lableGP5->setText(QString::number(ui->sldGPValue5->value()));

    threadAutomationFlag = 0;

    ui->tabWidget->setTabEnabled(3, false);

}

MainWindow::~MainWindow()
{
    delete ui;
    delete roboticsthread;
}

void MainWindow::on_txtValue1_textChanged()
{
}

//Setting Slider change event
void MainWindow::on_sldValue1_valueChanged(int value)
{
    ui->spValue1->setValue(value);

    roboticsthread->movePlinth(ui->spValue1->value());

    ui->sldGPValue1->setValue(value);

}

void MainWindow::on_sldValue2_valueChanged(int value)
{
    ui->spValue2->setValue(value);

    roboticsthread->moveJoint1(ui->spValue2->value());

     ui->sldGPValue2->setValue(value);
}

void MainWindow::on_sldValue3_valueChanged(int value)
{
    ui->spValue3->setValue(value);

    roboticsthread->moveJoint2(ui->spValue3->value());

     ui->sldGPValue3->setValue(value);
}

void MainWindow::on_sldValue4_valueChanged(int value)
{
    ui->spValue4->setValue(value);

    roboticsthread->moveNeck(ui->spValue4->value());

    ui->sldGPValue4->setValue(value);
}

void MainWindow::on_sldValue5_valueChanged(int value)
{
    ui->spValue5->setValue(value);

    roboticsthread->moveTongs(ui->spValue5->value());

     ui->sldGPValue5->setValue(value);
}

//Setting OK Button
void MainWindow::on_btnOK1_pressed()
{
    ui->sldValue1->setValue(ui->spValue1->value());

    roboticsthread->movePlinth(ui->spValue1->value());
}

void MainWindow::on_btnOK2_pressed()
{
    ui->sldValue2->setValue(ui->spValue2->value());

    roboticsthread->moveJoint1(ui->spValue2->value());
}

void MainWindow::on_btnOK3_pressed()
{
    ui->sldValue3->setValue(ui->spValue3->value());

    roboticsthread->moveJoint2(ui->spValue3->value());
}

void MainWindow::on_btnOK4_pressed()
{
    ui->sldValue4->setValue(ui->spValue4->value());

    roboticsthread->moveNeck(ui->spValue4->value());
}


void MainWindow::on_btnOK5_pressed()
{
    ui->sldValue5->setValue(ui->spValue5->value());

    roboticsthread->moveTongs(ui->spValue5->value());
}

void MainWindow::on_btnDoAll_pressed()
{
    ui->sldValue1->setValue(ui->spValue1->value());
    ui->sldValue2->setValue(ui->spValue2->value());
    ui->sldValue3->setValue(ui->spValue3->value());
    ui->sldValue4->setValue(ui->spValue4->value());
    ui->sldValue5->setValue(ui->spValue5->value());

    roboticsthread->moveAll(ui->spValue1->value(),
                            ui->spValue2->value(),
                            ui->spValue3->value(),
                            ui->spValue4->value(),
                            ui->spValue5->value());

}

// Button OK of Automatic's tab
void MainWindow::on_btnOKAuto1_pressed()
{
    ui->sldValueAuto1->setValue(ui->spValueAuto1->value());
}

void MainWindow::on_btnOKAuto2_pressed()
{
    ui->sldValueAuto2->setValue(ui->spValueAuto2->value());
}

void MainWindow::on_btnOKAuto3_pressed()
{
    ui->sldValueAuto3->setValue(ui->spValueAuto3->value());
}

void MainWindow::on_btnOKAuto4_pressed()
{
    ui->sldValueAuto4->setValue(ui->spValueAuto4->value());
}

void MainWindow::on_btnOKAuto5_pressed()
{
    ui->sldValueAuto5->setValue(ui->spValueAuto5->value());
}

//Setting Cancel Button
void MainWindow::on_btnCancel1_pressed()
{
    ui->spValue1->setValue(ui->sldValue1->value());
}

void MainWindow::on_btnCancel2_pressed()
{
    ui->spValue2->setValue(ui->sldValue2->value());
}

void MainWindow::on_btnCancel3_pressed()
{
    ui->spValue3->setValue(ui->sldValue3->value());
}

void MainWindow::on_btnCancel4_pressed()
{
    ui->spValue4->setValue(ui->sldValue4->value());
}

void MainWindow::on_btnCancel5_pressed()
{
    ui->spValue5->setValue(ui->sldValue5->value());
}

// Auto - slider value change
void MainWindow::on_sldValueAuto1_valueChanged(int value)
{
    ui->spValueAuto1->setValue(value);
}

void MainWindow::on_sldValueAuto2_valueChanged(int value)
{
    ui->spValueAuto2->setValue(value);
}

void MainWindow::on_sldValueAuto3_valueChanged(int value)
{
    ui->spValueAuto3->setValue(value);
}

void MainWindow::on_sldValueAuto4_valueChanged(int value)
{
    ui->spValueAuto4->setValue(value);
}

void MainWindow::on_sldValueAuto5_valueChanged(int value)
{
    ui->spValueAuto5->setValue(value);
}

void MainWindow::on_btnAutoStart_pressed()
{
    QAbstractItemModel* tableModel= ui->tableView->model();
    iRow = tableModel->rowCount();

    if(iRow <1)
    {
        QMessageBox mess;
        mess.setText("Please open file");
        mess.setStandardButtons(QMessageBox::Ok);
        mess.exec();
    }
    else {

        roboticsthread->tableModel = ui->tableView->model();
        if (!roboticsthread->isRunning())
        {
            roboticsthread->start();
            ui->tabWidget->setTabEnabled(0,false);
            ui->tabWidget->setTabEnabled(2,false);
            ui->btnAutoStart->setText("Stop");
            ui->tableView->setEnabled(false);
            ui->btnLoad->setEnabled(false);
            ui->btnSave->setEnabled(false);

        } else
        {
            roboticsthread->terminate();
            ui->tabWidget->setTabEnabled(0, true);
            ui->tabWidget->setTabEnabled(2, true);
            ui->btnAutoStart->setText("Start");
            ui->tableView->setEnabled(true);
            ui->btnLoad->setEnabled(true);
            ui->btnSave->setEnabled(true);
        }
    }
}



void MainWindow::on_btnSave_pressed()
{
    QAbstractItemModel* tableModel= ui->tableView->model();
    iRow = tableModel->rowCount();
    iCol = tableModel->columnCount();
    QString dataText;
    int posData[5];
    QString txt = ".txt";
    //QFile outputFile("../configFile/Results.txt");
    //outputFile.open(QIODevice::WriteOnly);
    QString fileName = QFileDialog::getSaveFileName(this,tr("Open File"), "", tr("Files(*.txt*)"));
    QFile outputFile(fileName+txt);
    outputFile.open(QIODevice::WriteOnly);

  if(outputFile.isOpen()){
   // Point a QTextStream object at the file
   QTextStream outStream(&outputFile);

   for (int i = 0; i<iRow; i++)
   {
       for (int j = 0; j<iCol; j++)
       {
           dataText = tableModel->index(i, j, QModelIndex()).data().toString();
           posData[j] = dataText.toInt();
       }
        outStream << QString::number(posData[0]) + " " + QString::number(posData[1]) + " "
                    + QString::number(posData[2]) + " "+ QString::number(posData[3]) + " "
                    + QString::number(posData[4]) + "\n";
   }
   outputFile.close();

} else qDebug() << "- Error, cannot open";

}

void MainWindow::on_btnLoad_pressed()
{
    //QAbstractItemModel* tableModel= ui->tableView->model();
    //iRow = tableModel->rowCount();

    QString fileName = QFileDialog::getOpenFileName(this,tr("Open File"), "", tr("Files(*.txt*)"));
    QAbstractItemModel *oldModel;

    QFile inputFile(fileName);
    int rowIndex = 0;
    if(inputFile.open(QIODevice::ReadOnly))
    {
        oldModel = ui->tableView->model();

        model = new QStandardItemModel(0,0,this);
        model->setHorizontalHeaderItem(0, new QStandardItem(QString("Value 1")));
        model->setHorizontalHeaderItem(1, new QStandardItem(QString("Value 2")));
        model->setHorizontalHeaderItem(2, new QStandardItem(QString("Value 3")));
        model->setHorizontalHeaderItem(3, new QStandardItem(QString("Value 4")));
        model->setHorizontalHeaderItem(4, new QStandardItem(QString("Value 5")));
        ui->tableView->setModel(model);

        if (oldModel != NULL)
        {
           delete oldModel;
           oldModel = NULL;
        }

        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QList<QStandardItem *> items;
            QStringList fields = line.split(" ");
            foreach (QString text, fields)
                items.append(new QStandardItem((QString)text));
            model->appendRow(items);

            rowIndex++;
        }
        inputFile.close();
    } else
    {
        QMessageBox::information(0,"error",inputFile.errorString());

    }
}


void MainWindow::on_btnExit_pressed()
{
    exit(0);
}

void MainWindow::delay()
{
    QTime dieTime= QTime::currentTime().addMSecs(70);
    while( QTime::currentTime() < dieTime )
    QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

void MainWindow::on_btnGPUp1_pressed()
{
    ui->btnGPDown1->setEnabled(false);
    flagGPValue2 = 0;

    for(gpValue2 = ui->sldGPValue2->value();gpValue2 > ui->sldGPValue2->minimum()-1; gpValue2 = gpValue2+7)
    {
        //ui->lableGP2->setText(QString::number(gpValue2));
        ui->sldGPValue2->setValue(gpValue2);

        roboticsthread->moveJoint1(ui->sldGPValue2->value());

        delay();
        if(flagGPValue2==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnGPUp1_released()
{
    ui->btnGPDown1->setEnabled(true);
    flagGPValue2 = 1;
}


void MainWindow::on_btnGPDown1_pressed()
{
    ui->btnGPUp1->setEnabled(false);

    flagGPValue2 = 0;

    for(gpValue2 = ui->sldGPValue2->value();gpValue2 < ui->sldGPValue2->maximum()+1; gpValue2 = gpValue2-7)
    {
        //ui->lableGP2->setText(QString::number(gpValue2));
        ui->sldGPValue2->setValue(gpValue2);

        roboticsthread->moveJoint1(ui->sldGPValue2->value());

        delay();
        if(flagGPValue2==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnGPDown1_released()
{
    ui->btnGPUp1->setEnabled(true);
    flagGPValue2 = 1;
}


void MainWindow::on_btnGPLeft_pressed()
{
    ui->btnGPRight->setEnabled(false);
    flagGPValue1 = 0;

    for(gpValue1 = ui->sldGPValue1->value();gpValue1 < ui->sldGPValue1->maximum()+1; gpValue1 = gpValue1+7)
    {
        //ui->lableGP1->setText(QString::number(gpValue1));
        ui->sldGPValue1->setValue(gpValue1);

        roboticsthread->movePlinth(ui->sldGPValue1->value());

        delay();
        if(flagGPValue1==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnGPLeft_released()
{
    ui->btnGPRight->setEnabled(true);
    flagGPValue1 = 1;
}


void MainWindow::on_btnGPRight_pressed()
{
    ui->btnGPLeft->setEnabled(false);
    flagGPValue1 = 0;

    for(gpValue1 = ui->sldGPValue1->value();gpValue1 > ui->sldGPValue1->minimum()-1; gpValue1=gpValue1-7)
    {
        //ui->lableGP1->setText(QString::number(gpValue1));
        ui->sldGPValue1->setValue(gpValue1);

        roboticsthread->movePlinth(ui->sldGPValue1->value());

        delay();
        if(flagGPValue1==1)
        {
            break;
        }
    }

}

void MainWindow::on_btnGPRight_released()
{
    ui->btnGPLeft->setEnabled(true);
    flagGPValue1 = 1;
}

void MainWindow::on_btnGPUp2_pressed()
{
    ui->btnGPDown2->setEnabled(false);
    flagGPValue3 = 0;

    for(gpValue3 = ui->sldGPValue3->value();gpValue3 > ui->sldGPValue3->minimum()-1; gpValue3=gpValue3+7)
    {
        //ui->lableGP3->setText(QString::number(gpValue3));
        ui->sldGPValue3->setValue(gpValue3);

        roboticsthread->moveJoint2(ui->sldGPValue3->value());

        delay();
        if(flagGPValue3==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnGPUp2_released()
{
    ui->btnGPDown2->setEnabled(true);
    flagGPValue3 = 1;
}

void MainWindow::on_btnGPDown2_pressed()
{
    ui->btnGPUp2->setEnabled(false);
    flagGPValue3 = 0;

    for(gpValue3 = ui->sldGPValue3->value();gpValue3 < ui->sldGPValue3->maximum()+1; gpValue3=gpValue3-7)
    {
        //ui->lableGP3->setText(QString::number(gpValue3));
        ui->sldGPValue3->setValue(gpValue3);

        roboticsthread->moveJoint2(ui->sldGPValue3->value());

        delay();
        if(flagGPValue3==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnGPDown2_released()
{
    ui->btnGPUp2->setEnabled(true);
    flagGPValue3 = 1;
}

void MainWindow::on_btnGPGrab_pressed()
{
    ui->btnGPDrop->setEnabled(false);
    if(ui->sldGPValue5->value() >= 205 && ui->sldGPValue5->value() < 225)
    {
        ui->sldGPValue5->setValue(225);
    } else if(ui->sldGPValue5->value() >= 225 && ui->sldGPValue5->value() < 250)
    {
        ui->sldGPValue5->setValue(250);
    } else if(ui->sldGPValue5->value() >= 250 && ui->sldGPValue5->value() < 275)
    {
        ui->sldGPValue5->setValue(275);
    } else if(ui->sldGPValue5->value() >= 275 && ui->sldGPValue5->value() < 300)
    {
        ui->sldGPValue5->setValue(300);
    } else if(ui->sldGPValue5->value() >= 300 && ui->sldGPValue5->value() < 325)
    {
        ui->sldGPValue5->setValue(325);
    } else if(ui->sldGPValue5->value() >= 325 && ui->sldGPValue5->value() < 350)
    {
        ui->sldGPValue5->setValue(350);
    } else if(ui->sldGPValue5->value() >= 350)
    {
        ui->sldGPValue5->setValue(375);
    }
}

void MainWindow::on_btnGPGrab_released()
{
    ui->btnGPDrop->setEnabled(true);
}

void MainWindow::on_btnGPDrop_pressed()
{
    ui->btnGPGrab->setEnabled(false);
    if(ui->sldGPValue5->value() > 350)
    {
        ui->sldGPValue5->setValue(350);
    } else if(ui->sldGPValue5->value() > 325 && ui->sldGPValue5->value() <= 350)
    {
        ui->sldGPValue5->setValue(325);
    } else if(ui->sldGPValue5->value() > 300 && ui->sldGPValue5->value() <= 325)
    {
        ui->sldGPValue5->setValue(300);
    } else if(ui->sldGPValue5->value() > 275 && ui->sldGPValue5->value() <= 300)
    {
        ui->sldGPValue5->setValue(275);
    } else if(ui->sldGPValue5->value() > 250 && ui->sldGPValue5->value() <= 275)
    {
        ui->sldGPValue5->setValue(250);
    } else if(ui->sldGPValue5->value() >225 && ui->sldGPValue5->value() <= 250)
    {
        ui->sldGPValue5->setValue(225);
    } else if(ui->sldGPValue5->value() <= 225 )
    {
        ui->sldGPValue5->setValue(205);
    }
}

void MainWindow::on_btnGPDrop_released()
{
    ui->btnGPGrab->setEnabled(true);

}

void MainWindow::on_btnGPRollLeft_pressed()
{
    ui->btnGPRollRight->setEnabled(false);
    if (ui->sldGPValue4->value()>=200 && ui->sldGPValue4->value() < 350)
    {
        ui->sldGPValue4->setValue(350);
    }else if (ui->sldGPValue4->value() >=350 && ui->sldGPValue4->value() < 512)
    {
        ui->sldGPValue4->setValue(512);
    }else if (ui->sldGPValue4->value() >=512 && ui->sldGPValue4->value() < 650)
    {
        ui->sldGPValue4->setValue(650);
    }else if (ui->sldGPValue4->value() >=650 && ui->sldGPValue4->value() < 800)
    {
        ui->sldGPValue4->setValue(800);
    }
}

void MainWindow::on_btnGPRollLeft_released()
{
    ui->btnGPRollRight->setEnabled(true);
}

void MainWindow::on_btnGPRollRight_pressed()
{
    ui->btnGPRollLeft->setEnabled(false);
    if (ui->sldGPValue4->value() <=800 && ui->sldGPValue4->value() > 650)
    {
            ui->sldGPValue4->setValue(650);
    }else if (ui->sldGPValue4->value() <=650 && ui->sldGPValue4->value() > 512)
    {
            ui->sldGPValue4->setValue(512);
    }else if (ui->sldGPValue4->value() <=512 && ui->sldGPValue4->value() > 350)
    {
            ui->sldGPValue4->setValue(350);
    }else if (ui->sldGPValue4->value() <=350 && ui->sldGPValue4->value() > 200)
    {
            ui->sldGPValue4->setValue(200);
    }
}

void MainWindow::on_btnGPRollRight_released()
{
    ui->btnGPRollLeft->setEnabled(true);
}

void MainWindow::on_sldGPValue1_valueChanged(int value)
{
    ui->lableGP1->setText(QString::number(value));
    ui->sldValue1->setValue(value);
}

void MainWindow::on_sldGPValue2_valueChanged(int value)
{
    ui->lableGP2->setText(QString::number(value));
    ui->sldValue2->setValue(value);
}

void MainWindow::on_sldGPValue3_valueChanged(int value)
{
    ui->lableGP3->setText(QString::number(value));
    ui->sldValue3->setValue(value);
}

void MainWindow::on_sldGPValue4_valueChanged(int value)
{
    ui->lableGP4->setText(QString::number(value));
    ui->sldValue4->setValue(value);
}

void MainWindow::on_sldGPValue5_valueChanged(int value)
{
    ui->lableGP5->setText(QString::number(value));
    ui->sldValue5->setValue(value);
}

void MainWindow::on_btnGPStart_clicked()
{
    if (flagGPStart ==0)
    {
        ui->btnGPUp1->setEnabled(true);
        ui->btnGPDown1->setEnabled(true);
        ui->btnGPLeft->setEnabled(true);
        ui->btnGPRight->setEnabled(true);
        ui->btnGPUp2->setEnabled(true);
        ui->btnGPDown2->setEnabled(true);
        ui->btnGPDrop->setEnabled(true);
        ui->btnGPGrab->setEnabled(true);
        ui->btnGPRollLeft->setEnabled(true);
        ui->btnGPRollRight->setEnabled(true);

        ui->btnGPStart->setText("Stop");

        ui->tabWidget->setTabEnabled(0, false);
        ui->tabWidget->setTabEnabled(1, false);

        flagGPStart = 1;
    } else
    {
        ui->btnGPUp1->setEnabled(false);
        ui->btnGPDown1->setEnabled(false);
        ui->btnGPLeft->setEnabled(false);
        ui->btnGPRight->setEnabled(false);
        ui->btnGPUp2->setEnabled(false);
        ui->btnGPDown2->setEnabled(false);
        ui->btnGPDrop->setEnabled(false);
        ui->btnGPGrab->setEnabled(false);
        ui->btnGPRollLeft->setEnabled(false);
        ui->btnGPRollRight->setEnabled(false);

        ui->btnGPStart->setText("Start");

        ui->tabWidget->setTabEnabled(0, true);
        ui->tabWidget->setTabEnabled(1, true);
        flagGPStart = 0;
    }
}

void MainWindow::on_btnHelp_clicked()
{
    ui->tabWidget->setTabEnabled(0, false);
    ui->tabWidget->setTabEnabled(1, false);
    ui->tabWidget->setTabEnabled(2, false);
    ui->tabWidget->setTabEnabled(3, true);
}

void MainWindow::on_btnCloseHelp_clicked()
{
    ui->tabWidget->setTabEnabled(0, true);
    ui->tabWidget->setTabEnabled(1, true);
    ui->tabWidget->setTabEnabled(2, true);
    ui->tabWidget->setTabEnabled(3, false);
}


void MainWindow::on_btnValue11_pressed()
{
    int vl1;
    flagGPValue1 = 0;
    for(vl1 = ui->spValue1->value();vl1 < ui->spValue1->maximum()+1; vl1=vl1+1)
    {
        //ui->lableGP1->setText(QString::number(gpValue1));
        ui->spValue1->setValue(vl1);

        delay();
        if(flagGPValue1==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnValue11_released()
{
    flagGPValue1 = 1;
}

void MainWindow::on_btnValue10_pressed()
{
    int vl10;
    flagGPValue1 = 0;
    for(vl10 = ui->spValue1->value();vl10 > ui->spValue1->minimum()-1; vl10=vl10-1)
    {
        ui->spValue1->setValue(vl10);

        delay();
        if(flagGPValue1==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnValue10_released()
{
    flagGPValue1 = 1;
}

void MainWindow::on_btnValue21_pressed()
{
    int vl2;
    flagGPValue2 = 0;
    for(vl2 = ui->spValue2->value();vl2 < ui->spValue2->maximum()+1; vl2=vl2+1)
    {
        ui->spValue2->setValue(vl2);

        delay();
        if(flagGPValue2==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnValue21_released()
{
    flagGPValue2 = 1;
}

void MainWindow::on_btnValue20_pressed()
{
    int vl20;
    flagGPValue2 = 0;
    for(vl20 = ui->spValue2->value();vl20 > ui->spValue2->minimum()-1; vl20=vl20-1)
    {
        ui->spValue2->setValue(vl20);

        delay();
        if(flagGPValue2==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnValue20_released()
{
    flagGPValue2 = 1;
}

void MainWindow::on_btnValue31_pressed()
{
    int vl3;
    flagGPValue3 = 0;
    for(vl3 = ui->spValue3->value();vl3 < ui->spValue3->maximum()+1; vl3=vl3+1)
    {
        ui->spValue3->setValue(vl3);

        delay();
        if(flagGPValue3==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnValue31_released()
{
    flagGPValue3 = 1;
}

void MainWindow::on_btnValue30_pressed()
{
    int vl30;
    flagGPValue3 = 0;
    for(vl30 = ui->spValue3->value();vl30 > ui->spValue3->minimum()-1; vl30=vl30-1)
    {
        ui->spValue3->setValue(vl30);

        delay();
        if(flagGPValue3==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnValue30_released()
{
    flagGPValue3 = 1;
}

void MainWindow::on_btnValue41_pressed()
{
    int vl4;
    flagGPValue4 = 0;
    for(vl4 = ui->spValue4->value();vl4 < ui->spValue4->maximum()+1; vl4=vl4+1)
    {
        ui->spValue4->setValue(vl4);

        delay();
        if(flagGPValue4==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnValue41_released()
{
    flagGPValue4 = 1;
}

void MainWindow::on_btnValue40_pressed()
{
    int vl40;
    flagGPValue4 = 0;
    for(vl40 = ui->spValue4->value();vl40 > ui->spValue4->minimum()-1; vl40=vl40-1)
    {
        ui->spValue4->setValue(vl40);

        delay();
        if(flagGPValue4==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnValue40_released()
{
    flagGPValue4 = 1;
}

void MainWindow::on_btnValue51_pressed()
{
    int vl5;
    flagGPValue5 = 0;
    for(vl5 = ui->spValue5->value();vl5 < ui->spValue5->maximum()+1; vl5=vl5+1)
    {
        ui->spValue5->setValue(vl5);

        delay();
        if(flagGPValue5==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnValue51_released()
{
    flagGPValue5 = 1;
}

void MainWindow::on_btnValue50_pressed()
{
    int vl50;
    flagGPValue5 = 0;
    for(vl50 = ui->spValue5->value();vl50 > ui->spValue5->minimum()-1; vl50=vl50-1)
    {
        ui->spValue5->setValue(vl50);

        delay();
        if(flagGPValue5==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnValue50_released()
{
    flagGPValue5 = 1;
}

void MainWindow::on_btnSp11_pressed()
{
    int sp1;
    flagGPValue1 = 0;
    for(sp1 = ui->spSpeed1->value();sp1 < ui->spSpeed1->maximum()+1; sp1=sp1+1)
    {
        ui->spSpeed1->setValue(sp1);

        delay();
        if(flagGPValue1==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnSp11_released()
{
    flagGPValue1 = 1;
}

void MainWindow::on_btnSp10_pressed()
{
    int sp0;
    flagGPValue1 = 0;
    for(sp0 = ui->spSpeed1->value();sp0 > ui->spSpeed1->minimum()-1; sp0=sp0-1)
    {
        ui->spSpeed1->setValue(sp0);

        delay();
        if(flagGPValue1==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnSp10_released()
{
    flagGPValue1 = 1;
}

void MainWindow::on_btnSp11_2_pressed()
{
    int sp2;
    flagGPValue2 = 0;
    for(sp2 = ui->spSpeed2->value();sp2 < ui->spSpeed2->maximum()+1; sp2=sp2+1)
    {
        ui->spSpeed2->setValue(sp2);

        delay();
        if(flagGPValue2==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnSp11_2_released()
{
    flagGPValue2 = 1;
}

void MainWindow::on_btnSp10_2_pressed()
{
    int sp20;
    flagGPValue2 = 0;
    for(sp20 = ui->spSpeed2->value();sp20 > ui->spSpeed2->minimum()-1; sp20=sp20-1)
    {
        ui->spSpeed2->setValue(sp20);

        delay();
        if(flagGPValue2==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnSp10_2_released()
{
    flagGPValue2 = 1;
}

void MainWindow::on_btnSp11_3_pressed()
{
    int sp3;
    flagGPValue3 = 0;
    for(sp3 = ui->spSpeed3->value();sp3 < ui->spSpeed3->maximum()+1; sp3=sp3+1)
    {
        ui->spSpeed3->setValue(sp3);

        delay();
        if(flagGPValue3==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnSp11_3_released()
{
    flagGPValue3 = 1;
}

void MainWindow::on_btnSp10_3_pressed()
{
    int sp30;
    flagGPValue3 = 0;
    for(sp30 = ui->spSpeed3->value();sp30 > ui->spSpeed3->minimum()-1; sp30=sp30-1)
    {
        ui->spSpeed3->setValue(sp30);

        delay();
        if(flagGPValue3==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnSp10_3_released()
{
    flagGPValue3 = 1;
}

void MainWindow::on_btnSp11_4_pressed()
{
    int sp4;
    flagGPValue4 = 0;
    for(sp4 = ui->spSpeed4->value();sp4 < ui->spSpeed4->maximum()+1; sp4=sp4+1)
    {
        ui->spSpeed4->setValue(sp4);

        delay();
        if(flagGPValue4==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnSp11_4_released()
{
    flagGPValue4 = 1;
}

void MainWindow::on_btnSp10_4_pressed()
{
    int sp40;
    flagGPValue4 = 0;
    for(sp40 = ui->spSpeed4->value();sp40 > ui->spSpeed4->minimum()-1; sp40=sp40-1)
    {
        ui->spSpeed4->setValue(sp40);

        delay();
        if(flagGPValue4==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnSp10_4_released()
{
    flagGPValue4 = 1;
}

void MainWindow::on_btnSp11_5_pressed()
{
    int sp5;
    flagGPValue5 = 0;
    for(sp5 = ui->spSpeed5->value();sp5 < ui->spSpeed5->maximum()+1; sp5=sp5+1)
    {
        ui->spSpeed5->setValue(sp5);

        delay();
        if(flagGPValue5==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnSp11_5_released()
{
    flagGPValue5 = 1;
}

void MainWindow::on_btnSp10_5_pressed()
{
    int sp50;
    flagGPValue5 = 0;
    for(sp50 = ui->spSpeed4->value();sp50 > ui->spSpeed4->minimum()-1; sp50=sp50-1)
    {
        ui->spSpeed5->setValue(sp50);

        delay();
        if(flagGPValue5==1)
        {
            break;
        }
    }
}

void MainWindow::on_btnSp10_5_released()
{
    flagGPValue5 = 1;
}

void MainWindow::on_spSpeed1_valueChanged(int value)
{
    roboticsthread->change_speed(MOTOR_PLINTH, value);
}

void MainWindow::on_spSpeed2_valueChanged(int value)
{
    roboticsthread->change_speed(MOTOR_JOINT1_1, value);
    roboticsthread->change_speed(MOTOR_JOINT1_2, value);
}

void MainWindow::on_spSpeed3_valueChanged(int value)
{
    roboticsthread->change_speed(MOTOR_JOINT2_1, value);
    roboticsthread->change_speed(MOTOR_JOINT2_2, value);
}

void MainWindow::on_spSpeed4_valueChanged(int value)
{
    roboticsthread->change_speed(MOTOR_NECK, value);
}

void MainWindow::on_spSpeed5_valueChanged(int value)
{
    roboticsthread->change_speed(MOTOR_TONGS, value);
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    roboticsthread->change_speed(MOTOR_GLOBAL_ID, 100);
}
