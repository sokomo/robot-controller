#-------------------------------------------------
#
# Project created by QtCreator 2013-10-14T03:58:04
# Make by TungNV60382
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HandRobotController
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    robotics.cpp

INCLUDEPATH += ../../include

LIBS += -L../lib -lrbbackend -ldxl -lm

HEADERS  += mainwindow.h \
    robotics.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=c++11

RESOURCES += \
    Images.qrc
