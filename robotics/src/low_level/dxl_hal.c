/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <sys/time.h>

#include "dxl_hal.h"

int	gSocket_fd	= -1;
long	glStartTime	= 0;
float	gRcvWaitTime	= 0.0f;
float	gByteTransTime	= 0.0f;

char	gDeviceName[20];

/*

DXL_HAL_OPEN_ERROR:
	dxl_hal_close();
	return 0;
}
*/


void dxl_hal_close()
{
	if(gSocket_fd != -1)
		close(gSocket_fd);
	gSocket_fd = -1;
}

int dxl_hal_open(char* deviceName, int baudNum)
{
  /* Device Option */
  float baudrate;
  baudrate = 2000000.0f / (float)(baudNum + 1);

  struct termios DevOptions;
  speed_t Speed;

  /* Open device */
  gSocket_fd = open(deviceName, O_RDWR | O_NOCTTY | O_NDELAY);
  /* Cannot open device */
  if (gSocket_fd == -1)
  {
    return 0;
  }

  /* Open the device in nonblocking mode */
  fcntl(gSocket_fd, F_SETFL, FNDELAY);

  /* Set parameters */
  tcgetattr(gSocket_fd, &DevOptions);                               /* Get the current options of the port */
  bzero(&DevOptions, sizeof(DevOptions));                           /* Clear all the options */

  switch (baudNum)                                                      /* Filter speed */
  {
  case 110  :     Speed=B110; break;
  case 300  :     Speed=B300; break;
  case 600  :     Speed=B600; break;
  case 1200 :     Speed=B1200; break;
  case 2400 :     Speed=B2400; break;
  case 4800 :     Speed=B4800; break;
  case 9600 :     Speed=B9600; break;
  case 19200 :    Speed=B19200; break;
  case 38400 :    Speed=B38400; break;
  case 57600 :    Speed=B57600; break;
  case 115200 :   Speed=B115200; break;
  case 1000000:   Speed=B1000000; break;
  default : return -4;
  }
  cfsetispeed(&DevOptions, Speed);                                       /* Set the baud rate at 115200 bauds */
  cfsetospeed(&DevOptions, Speed);
  DevOptions.c_cflag |= ( CLOCAL | CREAD |  CS8);                        /* Configure the device : 8 bits, no parity, no control */
  DevOptions.c_iflag |= ( IGNPAR | IGNBRK );
  DevOptions.c_oflag = 0;
  DevOptions.c_lflag = 0;
  DevOptions.c_cc[VTIME]=0;                                              /* Timer unused */
  DevOptions.c_cc[VMIN]=0;                                               /* At least on character before satisfy reading */
  tcsetattr(gSocket_fd, TCSANOW, &DevOptions);                           /* Activate the settings */

  gByteTransTime = 1000.0f / baudrate * 12.0f;
  return 1;
}


void dxl_hal_clear(void)
{
	tcflush(gSocket_fd, TCIFLUSH);
}

int dxl_hal_tx( unsigned char *pPacket, int numPacket )
{
	return write(gSocket_fd, pPacket, numPacket);
}

int dxl_hal_rx( unsigned char *pPacket, int numPacket )
{
	memset(pPacket, 0, numPacket);
	return read(gSocket_fd, pPacket, numPacket);
}

static inline long myclock()
{
	struct timeval tv;
	gettimeofday (&tv, NULL);
	return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
}

void dxl_hal_set_timeout( int NumRcvByte )
{
	glStartTime = myclock();
	gRcvWaitTime = gByteTransTime * (float) NumRcvByte + 5.0f;
	printf("\n Transfer Time: %f       Receive Wait Time: %f\n", gByteTransTime, gRcvWaitTime);
}

int dxl_hal_timeout(void)
{
	long time;

	time = myclock() - glStartTime;

	if(time > gRcvWaitTime)
		return 1;
	else if(time < 0)
		glStartTime = myclock();

	return 0;
}
