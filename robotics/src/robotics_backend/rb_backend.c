/* 
 * BSD 2-Clause License
 * Copyright (c) 2013, Duc Minh Tran
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice
 *     this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <termio.h>
#include <unistd.h>
#include <dynamixel.h>
#include <fcntl.h> /* Define declaration read write file  */
#include <errno.h>
#include <string.h>

#include <time.h>
#include <define_id_motors.h>


// Control table address
#define P_GOAL_POSITION_L        30
#define P_GOAL_POSITION_H       31

#define P_GOAL_SPEED_L		      32
#define P_GOAL_SPEED_H           33

#define P_TORQUE_L                   34
#define P_TORQUE_H                  35

#define P_PRESENT_POSITION_L	36
#define P_PRESENT_POSITION_H	37

#define P_MOVING		46

#define P_GOAL_LIMIT_L              0
#define P_GOAL_LIMIT_H              1023

extern int errno;

void print_error_message()
{
  fprintf(stderr, "Error is %s (errno=%d)\n",
          strerror(errno), errno);
}


int getDeviceInfoFile(char *deviceName, unsigned int *baudNum)
{
  int filefd = -1;
  unsigned int limitData = 100;
  char fileName[] = "deviceDXL.conf";
  char strData[limitData];

  if ((filefd = open(fileName, O_RDONLY)) < 0)
  {
    print_error_message();
    return -1;
  }

  if (read(filefd, strData, limitData) < 0)
  {
    print_error_message();
    close(filefd);
    return -1;
  }

  sscanf(strData, "%s %d", deviceName, baudNum);
  close(filefd);

  return 1;
}

void printCommStatus(int CommStatus)
{
  switch(CommStatus)
	{
	case COMM_TXFAIL:
		printf("COMM_TXFAIL: Failed transmit instruction packet!\n");
		break;

	case COMM_TXERROR:
		printf("COMM_TXERROR: Incorrect instruction packet!\n");
		break;

	case COMM_RXFAIL:
		printf("COMM_RXFAIL: Failed get status packet from device!\n");
		break;

	case COMM_RXWAITING:
		printf("COMM_RXWAITING: Now receiving status packet!\n");
		break;

	case COMM_RXTIMEOUT:
		printf("COMM_RXTIMEOUT: There is no status packet!\n");
		break;

	case COMM_RXCORRUPT:
		printf("COMM_RXCORRUPT: Incorrect status packet!\n");
		break;

	default:
		printf("This is unknown error code!\n");
		break;
	}
}

void printErrorCode()
{
  if(dxl_get_rxpacket_error(ERRBIT_VOLTAGE) == 1)
		printf("Input voltage error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_ANGLE) == 1)
		printf("Angle limit error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_OVERHEAT) == 1)
		printf("Overheat error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_RANGE) == 1)
		printf("Out of range error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_CHECKSUM) == 1)
		printf("Checksum error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_OVERLOAD) == 1)
		printf("Overload error!\n");

	if(dxl_get_rxpacket_error(ERRBIT_INSTRUCTION) == 1)
		printf("Instruction code error!\n");
}

int test_Comm_Status()
{
  int CommStatus;
  CommStatus = dxl_get_result();
  if( CommStatus == COMM_RXSUCCESS )
  {
    printErrorCode();
    return 1;
  }
  else
  {
    printCommStatus(CommStatus);
    return 0;
  }
}

void set_speed(int id, int value)
{
  dxl_write_word(id, P_GOAL_SPEED_L, value);
  test_Comm_Status();
}

int open_serial_device(char* deviceName, unsigned int baudNum)
{
  unsigned int baudNumFile = 0;
  char deviceNameFile[30];
  int flagResult = 0;

  if (getDeviceInfoFile(deviceNameFile, &baudNumFile) > 0)
    flagResult = dxl_initialize(deviceNameFile, baudNumFile);

  if (flagResult == 0)
    flagResult = dxl_initialize(deviceName, baudNum);

  if( flagResult == 0 )
	{
		fprintf(stderr, "Failed to open device %s\n", deviceName );
		/* printf( "Press Enter key to terminate...\n" );
		getchar(); */
		return 0;
	}
	else
		printf( "Succeed to open USB2Dynamixel!\n" );
  return 1;
}

void close_serial_device()
{
  dxl_terminate();
}

int check_motor_moving(int id)
{
  int Moving = -1;

  Moving = dxl_read_byte( id, P_MOVING );
  if (!test_Comm_Status())
    return -1;

  return Moving;
}



int rotate_motor(int id, int value)
{
  //int GoalPos[2] = {0, 4095}; // for Ex series

  dxl_write_word( id, P_GOAL_POSITION_L, value );
  if (!test_Comm_Status())
    return -2;

  return 1;
}


int rotate_multi_motors(int* idList, int numOfId, int* value)
{
  int index = 0;
  int dataLength = 2;

	/* Make syncwrite packet */
  dxl_set_txpacket_id(BROADCAST_ID);
  dxl_set_txpacket_instruction(INST_SYNC_WRITE);

  dxl_set_txpacket_parameter(0, P_GOAL_POSITION_L);
  dxl_set_txpacket_parameter(1, dataLength);

  for (index = 0; index < numOfId; index++)
  {
    dxl_set_txpacket_parameter(2 + 3 * index, *(idList + index));
    dxl_set_txpacket_parameter(2 + 3 * index + 1, dxl_get_lowbyte(*(value + index)));
    dxl_set_txpacket_parameter(2 + 3 * index + 2, dxl_get_highbyte(*(value + index)));
  }
  dxl_set_txpacket_length((dataLength + 1) * numOfId + 4);

  dxl_txrx_packet();
  return 1;
}


int rotate_multi_motors_same_value(int* idList, int numOfId, int value)
{
  int speedValue[numOfId];
  int index = 0;

  for (index = 0; index < numOfId; index++)
    speedValue[index] = value;

  rotate_multi_motors(idList, numOfId, speedValue);

  return 1;
}


int get_current_pos(int id)
{
    int curPos = -1;
    curPos = dxl_read_word(id, P_PRESENT_POSITION_L);
    if (!test_Comm_Status())
        return -1;

    return curPos;
}


/* High level communication */
void move_plinth_high_level(int pos)
{
  rotate_motor(MOTOR_PLINTH, pos);
}


void move_joints_high_level(int posJoint1, int posJoint2)
{
  int idList[4] = { MOTOR_JOINT1_1, MOTOR_JOINT1_2, MOTOR_JOINT2_1, MOTOR_JOINT2_2 };
  int posList[4] = {posJoint1 + 61, posJoint1,  posJoint2, posJoint2};
  int sizeArray = sizeof(idList) / sizeof(int);
  rotate_multi_motors(idList, sizeArray, posList);
}

void move_neck_high_level(int pos)
{
  rotate_motor(MOTOR_NECK, pos);
}


void move_tongs_high_level(int pos)
{
  rotate_motor(MOTOR_TONGS, pos);
}

void step_moving_automation(int posPlinth, int posJoint1, int posJoint2, int posNeck, int posTongs)
{
  move_plinth_high_level(posPlinth);
  sleep(1);
  move_joints_high_level(posJoint1, posJoint2);
  sleep(1);
  move_neck_high_level(posNeck);
  sleep(1);
  move_tongs_high_level(posTongs);
  sleep(1);
}
